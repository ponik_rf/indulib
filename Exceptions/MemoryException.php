<?php

namespace Ponikrf\Indulib\Exceptions;

/**
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
*/
class MemoryException extends IndulibException
{
    const codes = [
        3501 => 'Не удалось распознать формат памяти',
    ];
}
