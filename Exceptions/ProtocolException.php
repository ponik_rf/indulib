<?php

namespace Ponikrf\Indulib\Exceptions;


/**
 * Исключения для обработки ошибок связанные с работой провайдера
 *  @author Boris Bobylev <ponik_rf@mail.ru>
 */
class ProtocolException extends IndulibException
{
    protected $model = false;
    protected $data = [];

    const codes = [
        3800 => 'Внутренняя ошибка протокола',
        3801 => 'Внутренняя ошибка устройства',
        3802 => 'Ответ не был получен (Timeout)',
        3803 => 'Неизвестная команда',
        3804 => 'Неверный формат полученых данных',
        3805 => 'Некорректно сформирован запрос',
    ];


    /**
     * Текстовая информация о ошибке
     *
     * @param $text
     * @return ProtocolException
     */
    public function setData($text){
        $this->data = $text;
        return $this;
    }

    /**
     * Возвращает данные
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Назначает модель
     *
     * @param $model
     * @return ProtocolException
     */
    public function setModel($model){
        $this->model = $model;
        return $this;
    }

    /**
     * Возвращает модель
     */
    public function getModel()
    {
        return $this->model;
    }
}
