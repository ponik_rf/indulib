<?php

namespace Ponikrf\Indulib\Exceptions;
/**
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class IndulibException extends \Exception
{
    const codes = [];

    public static function create($code, $additional = ''){
        return new static(static::codes[$code],$code);
    }

    protected $model = false;
    protected $data = '';

    /**
     * Устанавливает дополнительные данные
     *
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Возвращает данные
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Устанавливает модель
     *
     * @param $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Возвращает модель
     */
    public function getModel()
    {
        return $this->model;
    }
}
