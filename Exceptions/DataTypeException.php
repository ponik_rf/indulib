<?php

namespace Ponikrf\Indulib\Exceptions;
/**
 * @author Boris Bobylev <ponik_rf@mail.ru>
*/
class DataTypeException extends IndulibException
{
    const codes = [
        3201 => 'Не удалось распознать формат памяти',
    ];
}
