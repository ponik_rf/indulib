<?php

namespace Ponikrf\Indulib\Exceptions;

/**
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class CastException extends IndulibException
{
    const codes = [
        3001 => 'Не удалось привести REGION SIZE к нормальному состоянию',
        3002 => 'REGION SIZE Выходит за пределы MEMORY',
        3003 => 'Размер был изменен после модификации',
        3004 => 'Ошибка модификации',
        3005 => 'CRC не соотвествует',
        3006 => 'Ошибка сверки CRC',
        3007 => 'Ошибка преобразование значения',
        3008 => 'Полученое значение не равно ожидаемому',
    ];
}