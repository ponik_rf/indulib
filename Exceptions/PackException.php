<?php

namespace Ponikrf\Indulib\Exceptions;

/**
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class PackException extends IndulibException
{
    const codes = [
        3101 => 'Не удалось упаковать значение',
        3102 => 'Размер упакованного значения равен 0',
        3103 => 'Размер упакованного значения не соответсвует размеру типа данных',
        3104 => 'Значение типа данных `data` не является строкой байт',
    ];
}