<?php

namespace Ponikrf\Indulib\Classes;

use Ponikrf\Indulib\Exceptions\CastException;
use Ponikrf\Indulib\Exceptions\MemoryException;

/**
 * Класс для сборки буффера байт
 *
 * @property Memory $Memory
 * @property Rule $Rule
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Cast
{
    /** CLASSES */
    protected $Memory;
    protected $Rule;

    /** Status cast */
    const STATUS_NO_CAST = 0;
    const STATUS_ERROR = -1;
    const STATUS_SUCCESS = 1;

    const STATUS = [
        0 => 'NO CAST',
        -1 => 'ERROR',
        1 => 'SUCCESS',
    ];

    protected $status = self::STATUS_NO_CAST;

    /** tmp vars */
    protected $render = [];
    protected $index = 0;
    protected $storage = [];
    protected $ruleArray = [];
    protected $error = null;

    /**
     * @param Memory $Memory
     * @param Rule $Rule
     * @param bool $autoCast
     */
    public function __construct(Memory $Memory, Rule $Rule, $autoCast = false)
    {
        $this->Memory = $Memory;
        $this->Rule = $Rule;
        if ($autoCast) $this->cast();
    }

    public function dump($return = false){
        $result = 'STATUS: '.self::STATUS[$this->status].PHP_EOL;
        $result .= 'MEMORY: '.(string)$this->Memory.PHP_EOL;
        $result .= 'RESULT: '.print_r($this->render,true);
        if($return) return $result;
        echo $result;
    }

    public function cast()
    {
        if ($this->status != self::STATUS_NO_CAST) {
            if ($this->status == self::STATUS_SUCCESS) return true;
            return false;
        }
        $this->index = 0;
        $this->ruleArray = [
            'type' => "Rule",
            'name' => "root",
            'count' => 1,
            'regions' => $this->Rule->toArray(),
        ];

        try {
            $this->renderRule($this->ruleArray, $this->render);
            $this->status = self::STATUS_SUCCESS;
            return true;
        } catch (\Exception $exception) {
            $this->error = $exception;
        }

        $this->status = self::STATUS_ERROR;
        return false;
    }

    /**
     *
     * Рекурсивная функция обработки правила
     *
     * @param $Rule
     * @param $render
     * @return bool
     * @throws CastException
     */
    public function renderRule(&$Rule, &$render)
    {
        foreach ($Rule['regions'] AS &$item) {
            switch ($item['type']) {
                case 'Rule':
                    $this->renderRule($item, $render[$item['name']]);
                    break;
                case 'Field':
                    if (array_key_exists('count', $item)) {
                        foreach ($item['regions'] AS $one) {
                            $this->renderField($one, $render[$item['name']][]);
                        }
                    } else {
                        $this->renderField($item, $render[$item['name']]);
                    }
                    break;
            }
        }
        return true;
    }

    /**
     *
     * Обработка поля
     *
     * @param $Field
     * @param $render
     * @return bool
     * @throws CastException
     */
    public function renderField(&$Field, &$render)
    {

        $this->fieldDefinitionSize($Field);
        try {
            $Field['regions']['bytes'] = $this->Memory->get($Field['regions']['size'], $this->index)->toString();
        } catch (MemoryException $exception) {
            throw CastException::create(3002); // OUT OF MEMORY
        }

        $this->fieldAddToStorage($Field);

        if ($Field['regions']['type'] == 'void') {
            $this->index += $Field['regions']['size'];
            return true;
        }

        if ($Field['regions']['type'] == 'data') {
            $render = $Field['regions']['bytes'];
            $this->index += $Field['regions']['size'];
            return true;
        }

        $this->fieldModify($Field);

        if (array_key_exists($Field['regions']['type'], DataType::CRCType)) {
            $this->fieldCRC($Field);
            $render = $Field['regions']['bytes'];
            $this->index += $Field['regions']['size'];
            return true;
        }

        if (in_array($Field['regions']['type'], DataType::dataTypes)) {

            try {
                $render = StringByte::cast($Field['regions']['type'], $Field['regions']['bytes']);
            } catch (\Exception $e) {
                throw CastException::create(3007);
            }

            if (is_array($Field['value'])){
                $check = false;
                foreach ($Field['value'] AS $value) if ($render == $value) {$check = true; break;}
                if (!$check) throw CastException::create(3008);
            }else{
                if ($Field['value'] != NULL AND $render != $Field['value'])
                    throw CastException::create(3008);
            }


            $Field['value'] = $render;

            $this->index += $Field['regions']['size'];
            return true;
        }
    }

    /**
     * Преобразует и проверяет размер
     *
     * @param $Field
     * @throws CastException
     */
    protected function fieldDefinitionSize(&$Field)
    {
        if (is_callable($Field['regions']['size'])) {
            $function = $Field['regions']['size'];
            try {
                $Field['regions']['size'] = $function($this->render);
            } catch (\Exception $exception) {
                throw CastException::create(3001);
            }
        }

        if (!is_numeric($Field['regions']['size']) OR $Field['regions']['size'] <= 0)
            throw CastException::create(3001);
    }

    /**
     * Добавляет данные в локальное хранилище
     *
     * @param $Field
     */
    protected function fieldAddToStorage(&$Field)
    {
        foreach ($Field['regions']['store'] AS $toStore) {
            if (!array_key_exists($toStore['store'], $this->storage)) $this->storage[$toStore['store']] = '';
            $this->storage[$toStore['store']] = StringByte::catBytes(
                $this->storage[$toStore['store']],
                $Field['regions']['bytes'],
                $toStore['ending']
            );
        }
    }

    /**
     * Модифицирует данные
     *
     * @param $Field
     * @throws CastException
     */
    protected function fieldModify(&$Field)
    {
        if (is_callable($Field['regions']['modify'])) {
            try {
                $function = $Field['regions']['modify'];
                $Field['regions']['original'] = $Field['regions']['bytes'];
                $Field['regions']['modified'] = true;
                $Field['regions']['bytes'] = $function($Field['regions']['bytes']);
            } catch (\Exception $e) {
                throw CastException::create(3004, $e->getMessage());
            }

            if ($Field['regions']['size'] != strlen($Field['regions']['bytes']))
                throw CastException::create(3003);
        }
    }

    /**
     *
     * @param $Field
     * @throws CastException
     */
    public function fieldCRC(&$Field)
    {
        try {
            $crc = CRC::crcMatch($Field['regions']['type'], $this->storage[$Field['name']], $Field['regions']['bytes']);
        } catch (\Exception $e) {
            throw CastException::create(3006, $e->getMessage());
        }
        if (!$crc) throw CastException::create(3005);
    }

    /**
     * Возвращет статус преобразования
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Получить результат приведения данных
     *
     * @return array
     */
    public function result()
    {
        return $this->render;
    }

    public function getError()
    {
        return $this->error;
    }


    /**
     * Получить результат приведения данных
     *
     * @return array
     */
    public function ruleArray()
    {
        return $this->ruleArray;
    }
}