<?php

namespace Ponikrf\Indulib\Classes;

/**
 * Класс для преобразования из массива байт в данные.
 *
 * Basic pack type:
 * @method static char($data)
 * @method static int8($data)
 * @method static uint8($data)
 * @method static int16($data)
 * @method static uint16($data)
 * @method static uint16_be($data)
 * @method static uint16_le($data)
 * @method static int32($data)
 * @method static uint32($data)
 * @method static uint32_be($data)
 * @method static uint32_le($data)
 * @method static int64($data)
 * @method static uint64($data)
 * @method static uint64_be($data)
 * @method static uint64_le($data)
 * @method static float($data)
 * @method static float_be($data)
 * @method static float_le($data)
 * @method static double($data)
 * @method static double_be($data)
 * @method static double_le($data)
 *
 * BCD type:
 * @method static BCD8($data)
 * @method static BCD16($data)
 * @method static BCD24($data)
 * @method static BCD32($data)
 * @method static BCD48($data)
 * @method static BCD64($data)
 *
 * CRC type:
 * @method static CRC8($data)
 * @method static CRC16($data)
 * @method static CRC32($data)
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class ArrayByteTo
{

    /**
     * Magic
     *
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Ponikrf\Indulib\Exceptions\DataTypeException
     */
    public static function __callStatic($name, $arguments)
    {
        return ArrayByte::cast($name,$arguments[0]);
    }

    /**
     * Конвертирует массив в строку
     *
     * @param array $array
     * @return string
     */
    public static function string(array $array) : string{
        return ArrayByte::toString($array);
    }
}