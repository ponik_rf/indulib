<?php

namespace Ponikrf\Indulib\Classes;

/**
 *
 * Класс определения области памяти
 *
 * @method self char($data, array $options = [])
 * @method self int8($data, array $options = [])
 * @method self uint8($data, array $options = [])
 * @method self int16($data, array $options = [])
 * @method self uint16($data, array $options = [])
 * @method self uint16_be($data, array $options = [])
 * @method self uint16_le($data, array $options = [])
 * @method self int32($data, array $options = [])
 * @method self uint32($data, array $options = [])
 * @method self uint32_be($data, array $options = [])
 * @method self uint32_le($data, array $options = [])
 * @method self int64($data, array $options = [])
 * @method self uint64($data, array $options = [])
 * @method self uint64_be($data, array $options = [])
 * @method self uint64_le($data, array $options = [])
 * @method self float($data, array $options = [])
 * @method self float_be($data, array $options = [])
 * @method self float_le($data, array $options = [])
 * @method self double($data, array $options = [])
 * @method self double_be($data, array $options = [])
 * @method self double_le($data, array $options = [])
 *
 * @method self BCD8($data, array $options = [])
 * @method self BCD16($data, array $options = [])
 * @method self BCD24($data, array $options = [])
 * @method self BCD32($data, array $options = [])
 * @method self BCD48($data, array $options = [])
 * @method self BCD64($data, array $options = [])
 *
 * @method self CRC8(array $options = [])
 * @method self CRC16(array $options = [])
 * @method self CRC32(array $options = [])
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Package
{
    protected $package = '';
    protected $toCRC = '';

    protected $packageArray = [];

    /**
     * Очищает пакет
     *
     * @return $this
     */
    public function clear()
    {
        $this->packageArray = [];
        return $this;
    }

    /**
     * Добавляет массив данных одного типа подряд
     *
     * используется если необходимо передать сразу массив данных одного типа
     *
     * Использование:
     *
     * ```PHP
     * $dataArray = [1,2,3,4,5] // Данные укладываемые в формат uint8
     * $Package->addArray("uint8",$dataArray);
     *
     * ```
     *
     * @param string $type
     * @param $array
     * @param $options
     * @return Package
     */
    public function addArray(string $type, $array, $options)
    {
        foreach ($array AS $item) {
            $this->packageArray[] = [
                'type' => $type,
                'data' => $item,
                'options' => $options,
            ];
        }
        return $this;
    }

    /**
     * Возвращает массив байт
     *
     * @return array
     * @throws \Ponikrf\Indulib\Exceptions\DataTypeException
     */
    public function toArray(): array
    {
        return StringByte::toArray($this->result());
    }

    /**
     * Возвращает строку байт
     *
     * @return string
     * @throws \Ponikrf\Indulib\Exceptions\DataTypeException
     */
    public function toString(): string
    {
        return $this->result();
    }

    /**
     * Добавляем строковые данные в пакет
     *
     * @param string $data
     * @param array $options
     * @return Package
     * @throws \Ponikrf\Indulib\Exceptions\DataTypeException
     */
    public function stringData(string $data, array $options = [])
    {
        $this->toPackage('string', $data, $options);
        return $this;
    }

    /**
     *
     *
     * @param array $data
     * @param array $options
     * @return Package
     */
    public function arrayData(array $data, array $options = [])
    {
        $this->toPackage('array', $data, $options);
        return $this;
    }

    /**
     * Выводит или возвращает дамп сформированного пакета
     *
     * @param bool $return
     * @return mixed|string
     * @throws \Ponikrf\Indulib\Exceptions\DataTypeException
     */
    public function dump($return = false)
    {

        $Memory = new Memory($this->toString());
        $MemoryDump = new MemoryDump($Memory);

        $params = '';
        // HEADER
        $params .= Console::sprint("   PACKAGE", 'white', false, 'bold');
        $params .= PHP_EOL;

        // TABLE HEAD
        $hedFormat = Console::sprint("%10s %10s %37s %5s  ", 'white', false, 'cursive');

        $params .= sprintf($hedFormat,
            'DATA TYPE',
            'SIZE',
            'DATA',
            'CRC'
        );
        $params .= PHP_EOL;

        // TABLE
        $format = Console::sprint("%10s %10s %37s %5s", 'yellow', false, 'cursive');

        foreach ($this->packageArray AS $item) {

            $crc = '       ';

            if ($item['options']['crc']) {
                $crc = '   |   ';
            }

            if (!array_key_exists($item['type'],DataType::dataTypesSize)){
                if ($item['type']=="string") $size = strlen($item['data']);
                if ($item['type']=="array") $size = count($item['data']);
            }else{
                $size = DataType::dataTypesSize[$item['type']];
            }

            if ($item['type']=="string") $data = trim(StringByte::printBytes($item['data'],true));
            elseif ($item['type']=="array") $data = trim(ArrayByte::printBytes($item['data'],true));
            else $data = $item['data'];

            $params .= sprintf($format,
                $item['type'],
                $size,
                substr($data, 0, 36),
                $crc
            );

            $params .= PHP_EOL;

            $MemoryDump->addScope($size, false, 'bold');
        }

        $params .= PHP_EOL;

        $result = $MemoryDump->dump(true);
        $result .= PHP_EOL;
        $result .= $params;

        if ($return) {
            return $result;
        }
        echo $result;
    }


    /**
     *
     * @param $name
     * @param array $arguments
     * @return $this
     * @throws \Exception
     */
    public function __call($name, $arguments = [])
    {
        if (array_key_exists($name, DataType::CRCType)) {
            $data = '';
            if (array_key_exists(0, $arguments)) $options = $arguments[0]; else $options = [];
        } else if (array_key_exists(0, $arguments)) {
            if (array_key_exists($name, DataType::dataTypes)) {
                throw new \Exception("This data type ($name) not found, see DataType::dataTypes");
            }
            $data = $arguments[0];
            if (array_key_exists(1, $arguments)) $options = $arguments[1]; else $options = [];
        } else throw new \Exception("The number of arguments does not match for name ($name) " . print_r($arguments, true));

        $this->toPackage($name,$data,$options);

        return $this;
    }

    protected function toPackage($type,$data,$options){

        if (!array_key_exists('crc', $options)) {
            $options['crc'] = true;
        }

        $this->packageArray[] = [
            'type' => $type,
            'data' => $data,
            'options' => $options,
        ];
    }

    /**
     * Рендерит один элемент пакета
     *
     * @param $item
     * @return string
     * @throws \Ponikrf\Indulib\Exceptions\DataTypeException
     */
    protected function renderOne($item)
    {

        if ($item['type'] == 'string') {
            return $item['data'];
        }

        if ($item['type'] == 'array') {
            return ArrayByte::toString($item['data']);
        }

        return StringByte::pack($item['type'], $item['data']);
    }


    /**
     *
     * Рендерит строку на основе package array
     *
     * @return string
     * @throws \Ponikrf\Indulib\Exceptions\DataTypeException
     */
    protected function result()
    {
        $crc = '';
        $result = '';
        foreach ($this->packageArray AS $item) {
            if (array_key_exists($item['type'], DataType::CRCType)) {
                $result .= CRC::crc($item['type'], $crc);
            } else {
                $render = $this->renderOne($item);
                if ($item['options']['crc']) {
                    $crc .= $render;
                }
                $result .= $render;
            }

        }
        return $result;
    }
}