<?php

namespace Ponikrf\Indulib\Classes;

use Ponikrf\Indulib\Exceptions\DataTypeException;

/**
 * Класс для работы с байтами, представленными в виде строки,
 * каждый элемент строки является отдельным байтом.
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class StringByte
{

    /**
     *  Преобразует строку байт в указанный тип.
     *
     * @param string $type
     * @param string $dataBytes
     * @return mixed
     * @throws DataTypeException
     */
    public static function cast(string $type, string $dataBytes)
    {
        // Обработка стандартных типов данных
        if (array_key_exists($type, DataType::packType))
            return unpack(DataType::packType[$type], $dataBytes)[1];
        // Обработка BCD типа данных
        if (array_key_exists($type, DataType::BCDType))
            return BCD::cast($dataBytes, DataType::BCDType[$type]);
        // Обработка CRC типа данных
        if (array_key_exists($type, DataType::CRCType))
            return $dataBytes; // CRC это и есть строка байт - ничего не делаем
        throw new DataTypeException("Data type [$type] not found");
    }

    /**
     * Преобразует строку байт в массив данных указанного типа.
     *
     * @param string $type
     * @param string $dataBytes
     * @return array
     * @throws DataTypeException
     */
    public static function castArray(string $type, string $dataBytes)
    {
        // Обработка стандартных типов данных
        if (array_key_exists($type, DataType::packType)) {
            $res = unpack(DataType::packType[$type] . '*', $dataBytes);
            $ret = [];
            foreach ($res as $c) $ret[] = $c;
            return $ret;
        }

        // Обработка BCD типа данных
        if (array_key_exists($type, DataType::BCDType)) {
            $bytesArray = self::getByteArray($dataBytes, DataType::BCDType[$type], floor(strlen($dataBytes) / DataType::BCDType[$type]));
            $result = [];
            foreach ($bytesArray as $bytes)
                $result[] = BCD::cast($bytes, DataType::BCDType[$type]);
            return $result;
        }

        // Обработка CRC типа данных (бессмысленно но надо что бы работало)
        if (array_key_exists($type, DataType::CRCType))
            return self::getByteArray($dataBytes, DataType::CRCType[$type], floor(strlen($dataBytes) / DataType::CRCType[$type]));

        throw new DataTypeException("Data type [$type] not found");
    }

    /**
     * Преобразует данные указанного типа в строку байт.
     *
     * @param string $type
     * @param $data
     * @return string
     * @throws DataTypeException
     */
    public static function pack(string $type, $data): string
    {
        // Обработка стандартных типов данных
        if (array_key_exists($type, DataType::packType))
            return pack(DataType::packType[$type], $data);


        // Обработка BCD типа данных
        if (array_key_exists($type, DataType::BCDType))
            return BCD::pack((int) $data, DataType::BCDType[$type]);

        // Обработка CRC типа данных
        if (array_key_exists($type, DataType::CRCType))
            return $data;
        throw new DataTypeException("Data type [$type] not found");
    }


    /**
     * Преобразует массив данных указанного типа в строку байт.
     *
     * @param string $type
     * @param array $dataArray
     * @return mixed
     * @throws DataTypeException
     */
    public static function packArray(string $type, array $dataArray): string
    {

        // Обработка стандартных типов данных
        if (array_key_exists($type, DataType::packType))
            return call_user_func_array('pack', array_merge([DataType::packType[$type] . '*'], $dataArray));


        // Обработка BCD типа данных
        if (array_key_exists($type, DataType::BCDType)) {
            $result = '';
            foreach ($dataArray as $data) $result .= BCD::pack($data, DataType::BCDType[$type]);
            return $result;
        }

        // Обработка CRC типа данных
        if (array_key_exists($type, DataType::CRCType)) {
            // По хорошему нужно проверять соответсвие размеру,
            // но это не задача упаковщика
            // CRC это и есть строка байт - ничего не делаем
            $result = '';
            foreach ($dataArray as $data) $result .= $data;
            return $result;
        }
        throw new DataTypeException("Data type [$type] not found");
    }

    /**
     * Печатает таблицу DEC HEX ASCII BIN для каждого байта.
     *
     * @param string $dataBytes
     * @param bool $return
     * @return string
     */
    public static function dump(string $dataBytes, $return = false)
    {
        $arrayByte = StringByteTo::array($dataBytes);
        $result = ArrayByte::dump($arrayByte, true);
        if ($return) return $result;
        echo $result;
        return '';
    }

    /**
     * Возвращает строку байт в количестве $countBytes и пропустив от начала $offsetBytes.
     *
     * @param string $dataBytes
     * @param int $countBytes
     * @param int $offsetBytes
     * @return string
     */
    public static function getBytes(string $dataBytes, int $countBytes = 1, int $offsetBytes = 0): string
    {
        return substr($dataBytes, $offsetBytes, $countBytes);
    }

    /**
     * Делит строку байт на части размером $countBytes в количестве $howMany
     * и возвращает результат в виде массива строк.
     *
     * @param string $dataBytes
     * @param int $countBytes
     * @param int $howMany
     * @return array
     */
    public static function getByteArray(string $dataBytes, int $countBytes = 1, int $howMany = 0): array
    {
        $r = [];
        for ($i = 0; $i < $howMany; $i++) $r[] = self::getBytes($dataBytes, $countBytes, $i * $countBytes);
        return $r;
    }

    /**
     * Генерирует строку случайных байт в количестве $Bytes.
     *
     * @param int $Bytes
     * @return string
     */
    public static function randomBytes(int $Bytes): string
    {
        return random_bytes($Bytes);
    }

    /**
     * Преобразует 2 строки байт в одну с порядком $ending
     *
     * @param string $a
     * @param string $b
     * @param bool $ending
     * @return string
     */
    public static function catBytes(string $a, string $b, bool $ending = true): string
    {
        if ($ending) return $a . $b; else return $b . $a;
    }

    /**
     * Возвращает количество байт
     *
     * @param string $dataBytes
     * @return int
     */
    public static function size(string $dataBytes): int
    {
        return strlen($dataBytes);
    }


    /**
     * Возвращает обратный порядок байт, группируя блоки байт размером $blockSize
     *
     * @param string $dataBytes
     * @param int $blockSize
     * @return string
     */
    public static function reverseBytes(string $dataBytes, int $blockSize = 1): string
    {
        if ($blockSize == 1) return strrev($dataBytes);
        $dataArray = self::getByteArray($dataBytes, $blockSize, (int) (self::size($dataBytes) / $blockSize));
        $res = '';
        foreach ($dataArray as $data) $res = self::catBytes($res, $data, false);
        return $res;
    }

    /**
     * Преобразует форматированную строку в строку байт используя в качестве разделителя $delimiter
     *
     * @param string $strBytes
     * @param string $delimiter
     * @return string
     * @internal param $stringByte
     */
    public static function fromFormatString(string $strBytes, string $delimiter = ':'): string
    {
        $r = '';
        $strArray = explode($delimiter, $strBytes);
        foreach ($strArray as $byte) $r .= pack('C', hexdec($byte));
        return $r;
    }

    /**
     * Печатает строку байт (или возвращает) ограниченную размером $size байт
     *
     * @param string $byteString
     * @param bool $return
     * @param int $size
     * @return string
     */
    public static function printBytes(string $byteString, bool $return = false, int $size = 0)
    {
        $byteArray = self::toArray($byteString);
        $result = ArrayByte::printBytes($byteArray, true, $size);
        if ($return) {
            return $result . PHP_EOL;
        } else {
            echo $result . PHP_EOL;
            return '';
        }
    }

    /**
     * Печатает (или возвращает) HEX символ байта
     *
     * @param string $byte
     * @param bool $return
     * @return string
     */
    public static function printByte(string $byte, bool $return = false): string
    {
        $byte = StringByteTo::uint8($byte);
        return ArrayByte::printByte($byte, $return);
    }

    /**
     * Возвращает true если считает что с точки зрения PHP
     * данная строка может преобразоватся в байт
     *
     * @param string $byte
     * @return bool
     */
    public static function isByte(string $byte): bool
    {
        if (strlen($byte) != 1) return false;
        return true;
    }

    /**
     * Преобразует строку байт в массив байт
     *
     * @param string $dataBytes
     * @return array
     */
    public static function toArray(string $dataBytes): array
    {
        try {
            $result = self::castArray('uint8', $dataBytes);
        } catch (DataTypeException $e) {
            $result = [];
        }
        return $result;
    }
}
