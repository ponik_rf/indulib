<?php

namespace Ponikrf\Indulib\Classes;

/**
 *
 * Абстракция поля куска памяти
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Field
{
    protected $filed = [
        'type' => '',
        'size' => '',
        'store' => [],
        'modify' => null,
        'modified' => null,
        'bytes' => null,
        'original' => null,
    ];

    /**
     * Возвращает тип поля
     */
    public function getType(){
        return $this->filed['type'];
    }

    /**
     * Возвращает размер поля
    */
    public function getSize(){
        return $this->filed['size'];
    }

    /**
     * При создании заполняется тип и размер
     *
     * @param string $type
     * @param int $size
     */
    public function __construct(string $type, $size)
    {
        $this->filed['type'] = $type;
        $this->filed['size'] = $size;
    }

    /**
     * Сообщает что эту область нужно сложить в отдельное хранилище
     *
     * Используется для CRC
     *
     * @param string $store
     * @param bool $ending
     * @return Field
     */
    public function store(string $store, bool $ending = true): self
    {
        $this->filed['store'][] = [
            'store' => $store,
            'ending' => $ending,
        ];
        return $this;
    }

    /**
     * Модификатор данных
     *
     * @todo определить однозначное поведение
     *
     * @param $modify
     * @return Field
     */
    public function modify($modify): self
    {
        $this->filed['modify'] = $modify;
        return $this;
    }

    /**
     * Возвращает результат настройки поля
     *
     * @return array
     */
    public function toArray(){
        return $this->filed;
    }
}