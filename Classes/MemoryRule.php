<?php

namespace Ponikrf\Indulib\Classes;

/**
 *
 * Класс определения области памяти
 *
 * @method self char(string $name, array $options = [])
 * @method self int8(string $name, array $options = [])
 * @method self uint8(string $name, array $options = [])
 * @method self int16(string $name, array $options = [])
 * @method self uint16(string $name, array $options = [])
 * @method self uint16_be(string $name, array $options = [])
 * @method self uint16_le(string $name, array $options = [])
 * @method self int32(string $name, array $options = [])
 * @method self uint32(string $name, array $options = [])
 * @method self uint32_be(string $name, array $options = [])
 * @method self uint32_le(string $name, array $options = [])
 * @method self int64(string $name, array $options = [])
 * @method self uint64(string $name, array $options = [])
 * @method self uint64_be(string $name, array $options = [])
 * @method self uint64_le(string $name, array $options = [])
 * @method self float(string $name, array $options = [])
 * @method self float_be(string $name, array $options = [])
 * @method self float_le(string $name, array $options = [])
 * @method self double(string $name, array $options = [])
 * @method self double_be(string $name, array $options = [])
 * @method self double_le(string $name, array $options = [])
 *
 * BCD type:
 * @method self BCD8(string $name, array $options = [])
 * @method self BCD16(string $name, array $options = [])
 * @method self BCD24(string $name, array $options = [])
 * @method self BCD32(string $name, array $options = [])
 * @method self BCD48(string $name, array $options = [])
 * @method self BCD64(string $name, array $options = [])
 *
 * CRC type:
 * @method self CRC8()
 * @method self CRC16()
 * @method self CRC32()
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class MemoryRule
{
    protected $rules = [];
    protected $binds = [];
    protected $tmpCount = 1;

    public function count($count)
    {
        $this->tmpCount = $count;
        return $this;
    }

    public function data(string $name, $size, array $options = [])
    {
        $options['size'] = $size;
        $options['name'] = $name;
        $options['type'] = 'data';
        $this->addRule($options);

        return $this;
    }

    public function __call($name, $arguments = [])
    {
        $options = [];
        if (count($arguments) == 2) {
            $options = $arguments[1];
        }elseif (count($arguments) > 2) {
            throw new \Exception("q");
        }

        if (count($arguments)>0){
            $options['name'] = $arguments[0];
        }

        $options['type'] = $name;

        if (in_array($name, DataType::dataTypes)) {
            //Встроенные типы данных
            $options['size'] = DataType::dataTypesSize[$name];
        }

        if (in_array($name, DataType::CRCType)) {
            //если указан CRC тип
            $options['size'] = DataType::dataTypesSize[$name];
            $options['CRC'] = false;
            $options['name'] = $options['type'];

        }

        return $this->addRule($options);
    }

    public function result()
    {
        return $this->rules;
    }

    /**
     * Добавить правило
     *
     *
     * @param $type
     * @param array $options
     * @return MemoryRule
     */
    protected function addRule(array $options = [])
    {
        if (!array_key_exists('CRC',$options)){
            $options['CRC'] = true;
        }

        if (!array_key_exists('bind',$options)){
            $options['bind'] = '';
        }

        if (!array_key_exists('values',$options)){
            $options['values'] = '';
        }

        if ($this->tmpCount > 0) {
            $options['count'] = $this->tmpCount;
            $this->tmpCount = 1;
        }
        $this->rules[] = $options;
        return $this;
    }
}