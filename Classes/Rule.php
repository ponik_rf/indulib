<?php

namespace Ponikrf\Indulib\Classes;

/**
 *
 * Класс определения области памяти
 *
 * @method Field char(string $name, int $count = 1, $values = null)
 * @method Field int8(string $name, int $count = 1, $values = null)
 * @method Field uint8(string $name, int $count = 1, $values = null)
 * @method Field int16(string $name, int $count = 1, $values = null)
 * @method Field uint16(string $name, int $count = 1, $values = null)
 * @method Field uint16_be(string $name, int $count = 1, $values = null)
 * @method Field uint16_le(string $name, int $count = 1, $values = null)
 * @method Field int32(string $name, int $count = 1, $values = null)
 * @method Field uint32(string $name, int $count = 1, $values = null)
 * @method Field uint32_be(string $name, int $count = 1, $values = null)
 * @method Field uint32_le(string $name, int $count = 1, $values = null)
 * @method Field int64(string $name, int $count = 1, $values = null)
 * @method Field uint64(string $name, int $count = 1, $values = null)
 * @method Field uint64_be(string $name, int $count = 1, $values = null)
 * @method Field uint64_le(string $name, int $count = 1, $values = null)
 * @method Field float(string $name, int $count = 1, $values = null)
 * @method Field float_be(string $name, int $count = 1, $values = null)
 * @method Field float_le(string $name, int $count = 1, $values = null)
 * @method Field double(string $name, int $count = 1, $values = null)
 * @method Field double_be(string $name, int $count = 1, $values = null)
 * @method Field double_le(string $name, int $count = 1, $values = null)
 *
 * BCD type:
 * @method Field BCD8(string $name, int $count = 1, $values = null)
 * @method Field BCD16(string $name, int $count = 1, $values = null)
 * @method Field BCD24(string $name, int $count = 1, $values = null)
 * @method Field BCD32(string $name, int $count = 1, $values = null)
 * @method Field BCD48(string $name, int $count = 1, $values = null)
 * @method Field BCD64(string $name, int $count = 1, $values = null)
 *
 * CRC type:
 * @method Field CRC8(string $store)
 * @method Field CRC16(string $store)
 * @method Field CRC32(string $store)
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Rule
{
    protected $rules = [];

    protected $size = 0;
    protected $_incrementSize = 1;

    public function getSize()
    {
        return $this->size;
    }

    /**
     * Добавляет область неопределенной памяти
     *
     *
     * @param string $name
     * @param $size
     * @param int $count
     * @param null $values
     * @return Field
     */
    public function data(string $name, $size, $count = 1, $values = null)
    {
        $newField = new Field('data', $size);
        $this->add('Field', $newField, $name, $count, $values);
        return $newField;
    }

    /**
     * Неиспользуемая область
     * @param $size
     * @param int $count
     * @return Field
     */
    public function void($size, $count = 1)
    {
        $newField = new Field('void', $size);
        $this->add('Field', $newField, 'void', $count);
        return $newField;
    }

    /**
     * Добавляет вложенное правило
     *
     * @param string $name
     * @param \Closure|Rule $rule
     * @param int $count
     * @return Rule
     */
    public function addRule(string $name, $rule, int $count = 1)
    {
        if (is_callable($rule)) {
            $newRule = new Rule();
            $rule($newRule);
            $this->add('Rule', $newRule, $name, $count);
            return $newRule;
        }

        if ($rule instanceof Rule) {
            $this->add('Rule', $rule, $name, $count);
            return $rule;
        }
    }

    /**
     * Магический метод для заполнеия полей правила
     *
     * @param $name
     * @param array $arguments
     * @return Field
     * @throws \Exception
     */
    public function __call($name, $arguments = [])
    {
        if (count($arguments) > 3) throw new \Exception("To many arguments");
        if (array_key_exists(0, $arguments)) $optionName = $arguments[0]; else $optionName = $name;
        if (array_key_exists(1, $arguments)) $optionCount = $arguments[1]; else $optionCount = 1;
        if (array_key_exists(2, $arguments)) $optionValues = $arguments[2]; else $optionValues = null;

        if (in_array($name, DataType::dataTypes)) {
            $newField = new Field($name, DataType::dataTypesSize[$name]);
            $this->add("Field", $newField, $optionName, $optionCount, $optionValues);
            return $newField;
        }
        throw new \Exception("Type not found");
    }

    /**
     *
     * Локальная функция добавления правила или поля в локальное хранилище
     *
     * @param $type
     * @param $part
     * @param $name
     * @param $count
     * @param null $values
     * @return null
     */
    protected function add($type, $part, $name, $count, $values = null)
    {
        if ($count <= 0) return null;

        if ($this->_incrementSize) {
            $size = $part->getSize();
            if (is_callable($size)) {
                $this->_incrementSize = 0;
                $this->size = null;
            } elseif ($this->size === null) {
                $this->_incrementSize = 0;
                $this->size = null;
            } else {
                $this->size = $size * $count;
            }
        }

        $this->rules[] = [
            'type' => $type,
            'name' => $name,
            'count' => $count,
            'part' => $part,
            'values' => $values,
        ];
    }

    /**
     * Преобразует правило в массив
     *
     *
     */
    public function toArray()
    {
        $result = [];
        foreach ($this->rules AS $rule) {

            $part = $rule['part']->toArray();

            if ($rule['count'] == 1) {
                $result[] = [
                    'type' => $rule['type'],
                    'name' => $rule['name'],
                    'value' => $rule['values'],
                    'regions' => $part,
                ];
            } elseif ($rule['count'] > 1) {
                $inputPart = [];

                for ($i = 0; $i < $rule['count']; $i++) {

                    $value = null;

                    if (is_array($rule['values'])) $value = array_key_exists($i, $rule['values']) ? $rule['values'][$i] : null;

                    $inputPart[] = [
                        'type' => $rule['type'],
                        'name' => $rule['name'],
                        'value' => $value,
                        'regions' => $part,
                    ];
                }

                $result[] = [
                    'type' => $rule['type'],
                    'name' => $rule['name'],
                    'count' => $rule['count'],
                    'value' => null,
                    'regions' => $inputPart,
                ];
            }

        }
        return $result;
    }
}