<?php

namespace Ponikrf\Indulib\Classes;

/**
 * Класс для работы с двоично десятичным кодом. (BCD формат)
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class BCD
{

    /**
     * Преобразует строку байт или массив байт в число
     * Можно ограничить количество используемых байт параметром $size
     *
     * @param $dataBytes
     * @param $size
     * @return int
     */
    public static function cast($dataBytes, int $size = 12)
    {
        if (is_string($dataBytes)) $dataBytes = StringByteTo::array($dataBytes);
        $r = '';
        $i = 0;
        foreach ($dataBytes as $byte) {
            if ($i >= $size) break;
            $r .= sprintf("%02x", $byte);
            $i++;
        }
        return (int) $r;
    }

    /**
     * Возвращает **строку** байт в формате BCD преобразованную из числа $number 
     * размером $size байт. Выравнивание числа происходит по правой стороне.
     *
     * @param $number
     * @param $size
     * @return string
     */
    public static function pack(int $number, int $size)
    {
        $strInt = (string) $number;
        $countAct = floor(strlen($strInt) / 2);
        $hfAct = (0 < (strlen($strInt) % 2));
        $r = '';
        for ($i = 1; $i <= $size; $i++) {
            if ($countAct >= $i) $k = hexdec((int) substr($strInt, $i * -2, 2));
            elseif ($countAct + 1 == $i and $hfAct) $k = hexdec((int) substr($strInt, ($i * -2) + 1, 1));
            else $k = 0;
            $r = pack('C', $k) . $r;
        }
        return $r;
    }
}
