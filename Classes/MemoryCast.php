<?php

namespace Ponikrf\Indulib\Classes;

use Ponikrf\Indulib\Exceptions\MemoryException;

/**
 * Класс для сборки буффера байт
 *
 * @property Memory $Memory
 * @property MemoryRule $Rule
 * @property MemoryDump $Dump
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class MemoryCast
{
    /** CLASSES */
    protected $Memory;
    protected $Rule;
    protected $Dump;

    /** Status cast */
    const STATUS_NO_CAST = 0;
    const STATUS_ERROR = -1;
    const STATUS_SUCCESS = 1;

    protected $status = self::STATUS_NO_CAST;

    /** tmp vars */
    protected $binds = [];
    protected $render = [];
    protected $casts = [];
    protected $CRC = '';
    protected $error = [];

    /**
     * @param Memory $Memory
     * @param MemoryRule $Rule
     * @param bool $autoCast
     */
    public function __construct(Memory $Memory, MemoryRule $Rule, $autoCast = false)
    {
        $this->Memory = $Memory;
        $this->Rule = $Rule;
        $this->Dump = new MemoryDump($Memory);
        if ($autoCast) $this->cast();
    }

    public function dump($return = false)
    {

        $result = PHP_EOL;
        $result .= Console::sprint("MEMORY DUMP", false, false, 'bold');
        $result .= " total memory size = " . $this->Memory->size();

        $result .= $this->Dump->dump(true);

        if ($this->status == self::STATUS_ERROR) {

            $result .= PHP_EOL;

            $result .= "\e[1;37m   RULE:\e[0m" . PHP_EOL;

            $result .= sprintf(
                "\e[4;33m%5s %10s %10s %15s %15s %7s\e[0m",
                'POS',
                'NAME',
                'TYPE',
                'BIND',
                'SIZE',
                'COUNT'
            );

            $result .= PHP_EOL;

            foreach ($this->Rule->result() AS $key => $rule) {
                $position = $key + 1;
                $format = "%5d %10s %10s %15s %15s %7d";
                if ($position == $this->error['position']) {
                    $format = "\e[4;31m%5d %10s %10s %15s %15s %7d\e[0m";
                }

                $result .= sprintf(
                    $format,
                    $position,
                    $rule['name'],
                    $rule['type'],
                    $rule['bind'],
                    is_callable($rule['size']) ? 'callable' : $rule['size'],
                    $rule['count']
                );

                $result .= PHP_EOL;
            }

            $result .= PHP_EOL;

            $result .= "\e[1;37m   CAST ERROR:  \e[0m" . PHP_EOL;


            $result .= sprintf(
                "\e[4;33m%5s %10s %10s %5s %7s\e[0m",
                'POS',
                'NAME',
                'TYPE',
                'SIZE',
                'ERROR'
            );

            $result .= PHP_EOL;

            $result .= sprintf(
                "%5d %10s %10s %5d   %s",
                $this->error['position'],
                $this->error['rule']['type'],
                $this->error['rule']['name'],
                $this->error['rule']['size'],
                $this->error['error']
            );

            $result .= PHP_EOL;

            //$result.=print_r($this->error['rule'],true);
        }
        if ($return) {
            return $result;

        }
        echo $result;

    }

    public function cast()
    {
        if ($this->status != self::STATUS_NO_CAST) {
            if ($this->status == self::STATUS_SUCCESS) return true;
            return false;
        }

        if ($this->tryCast()) {
            $this->status = self::STATUS_SUCCESS;
            return true;
        }

        $this->status = self::STATUS_ERROR;
        return false;
    }

    protected function tryCast()
    {
        $start = 0;
        foreach ($this->Rule->result() AS $key => $rule) {

            $position = $key + 1;

            /** заполняем биндинги */
            if (is_callable($rule['size'])) {
                $function = $rule['size'];
                $rule['size'] = $function($this->binds);
            }

            /** Проверка на получениие правильного размера */
            if (!is_numeric($rule['size']) OR $rule['size'] < 0) {
                $this->addError($position, $rule, "Could not bring the size to the desired form (size = '" . $rule['size'] . "')");
                return false;
            }

            $size = $rule['size'] * $rule['count'];

            /** Выделяем дамп памяти */
            $this->Dump->addScope($size, 'yellow', 'bold');

            /** Проверка на ноль */
            if ($size == 0) {
                $this->addError($position, $rule, "Size is zero");
                return false;
            }

            /************************************************
             *
             *  Выделяем байты и начинаем обработку данных
             *
             ************************************************/

//            try {
//                $bytes = $this->Memory->get($size, $start);
//            } catch (MemoryException $e) {
//                $this->addError($position, $rule, "The size out of memory (size = $size) residue bytes " . ($this->Memory->size() - $start));
//                return false;
//            }

            $bytes = StringByte::getBytes($this->Memory->toString(), $size, $start);

            /** закатываем CRC */
            if ($rule['CRC']) $this->CRC = StringByte::catBytes($this->CRC, $bytes);

            /** Применяем модификатор */
            if (array_key_exists('modify', $rule) AND is_callable($rule['modify'])) {
                try {
                    $function = $rule['modify'];
                    $bytes = $function($bytes);
                    if ($size != strlen($bytes)){
                        $this->addError($position, $rule, "Size after modify is different");
                        return false;
                    }
                } catch (\Exception $e) {
                    $this->addError($position, $rule, "Could not modify bind");
                    return false;
                }
            }

            /** кстуем Бинд */
            if (array_key_exists('bind', $rule) AND $rule['bind']) {
                try {
                    $this->binds[$rule['bind']] = StringByte::cast($rule['type'], $bytes);
                } catch (\Exception $e) {
                    $this->addError($position, $rule, "Could not cast bind");
                    return false;
                }
            }

            $value = '';

            /** Ищем в CRC типах */
            if (in_array($rule['type'], DataType::CRCType)) {

                try {
                    if (!CRC::crcMatch($rule['type'], $this->CRC, $bytes)) {
                        $crcBytes = CRC::crc($rule['type'], $this->CRC);
                        $this->addError($position, $rule, "CRC do not match. Was expected (" . trim(StringByte::printBytes($crcBytes, true)) . ")");
                        return false;
                    }
                } catch (\Exception $exception) {
                    $this->addError($position, $rule, $exception->getMessage());
                    return false;
                }

                $this->casts[$rule['name']] = $bytes;
                /** Ищем в стандартных типах */
            } elseif (in_array($rule['type'], DataType::dataTypes) AND $bytes) {

                if ($rule['count'] > 1) {
                    try {
                        $value = StringByte::castArray($rule['type'], $bytes);
                    } catch (\Exception $e) {
                        $this->addError($position, $rule, "Could not cast array value");
                        return false;
                    }
                } else {
                    try {
                        $value = StringByte::cast($rule['type'], $bytes);
                    } catch (\Exception $e) {
                        $this->addError($position, $rule, "Could not cast value");
                        return false;
                    }
                }
                $this->casts[$rule['name']] = $value;
            } else {
                $this->casts[$rule['name']] = $bytes;
            }

            if (!empty($rule['values'])) {
                $access = false;
                $errorAccess = [];
                foreach ($rule['values'] AS $value) {
                    if ($value == $this->casts[$rule['name']]) {
                        $access = true;
                        $errorAccess = "";
                        break;
                    }
                    if (is_string($value)) {
                        $errorAccess[] = StringByte::printBytes($this->casts[$rule['name']]);
                    } else {
                        $errorAccess[] = $this->casts[$rule['name']];
                    }
                }

                if (!$access) {
                    if (is_string($this->casts[$rule['name']])) {
                        $cast = StringByte::printBytes($this->casts[$rule['name']]);
                    } else {
                        $cast = $this->casts[$rule['name']];
                    }

                    $expect = "[" . implode(",", $errorAccess) . "]";
                    $this->addError($position, $rule, "Expected value does not match (get value [$cast]) expect [$expect]");
                }
            }

            $this->render[] = [
                'name' => $rule['name'],
                'type' => $rule['type'],
                'value' => $value,
                'bin' => decbin($value),
                'bytes' => trim(StringByte::printBytes($bytes, true, 8)),
                'size' => $size,
            ];

            $start += $size;
        }

        if ($start < $this->Memory->size()) {
            $this->addError($position, $rule, "Memory more rule");
            return false;
        }

        return true;
    }

    /**
     * Возвращет статус преобразования
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Получить результат приведения данных
     *
     * @return array
     */
    public function result()
    {
        return $this->casts;
    }

    /**
     * Получение ошибки
     */
    public function getError()
    {
        if (empty($this->error)) {
            return false;
        }
        return $this->error;
    }

    public function getDump()
    {
        return $this->Dump;
    }

    protected function addError($position, $rule, $error)
    {
        $this->error = [
            'position' => $position,
            'rule' => $rule,
            'error' => $error,
        ];
    }

}