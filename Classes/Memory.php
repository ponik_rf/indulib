<?php

namespace Ponikrf\Indulib\Classes;

use Ponikrf\Indulib\Exceptions\MemoryException;
use Ponikrf\Indulib\Exceptions\CastException;

/**
 * Класс для сборки буффера байт
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Memory
{
    protected $blocks;
    protected $render;

    protected $start_address;
    protected $memory;

    /**
     * @param string $memory
     * @throws MemoryException
     */
    public function __construct($memory = '')
    {
        $this->set($memory);
    }


    /**
     * Назначить память
     *
     * @param string $memory Строка или массив числовых байт байт
     * @return Memory
     * @throws MemoryException
     */
    public function set($memory = '')
    {
        if (is_array($memory))
            $this->memory = ArrayByteTo::string($memory);
        elseif (is_string($memory))
            $this->memory = $memory;
        else
            throw MemoryException::create(3501);
        return $this;
    }

    /**
     * Возвращает размер памяти
     *
     * @return integer
     */
    public function size()
    {
        return strlen($this->memory);
    }

    /**
     * Возвращает память в виде массива
     *
     * @return array
     */
    public function toArray()
    {
        return ArrayByteFrom::string($this->memory);
    }

    /**
     *  Возвращает память в виде строки
     *
     * @return string
     */
    public function toString()
    {
        return $this->memory;
    }

    /**
     * Разделяет память на 2 части, вторую часть возвращает
     * первая часть будет уреназа до размера $size
     *
     * @param int $size
     * @return Memory
     * @throws MemoryException
     */
    public function split(int $size)
    {
        if ($size > $this->size()) {
            throw new MemoryException("Split size out of memory", 97001);
        }
        $start = StringByte::getBytes($this->memory, $size);
        $endMem = new Memory(StringByte::getBytes($this->memory, $this->size() - $size, $size));
        $this->set($start);
        return $endMem;
    }

    /**
     * Взять часть памяти
     *
     * @param int $count
     * @param int $offset
     * @return Memory
     * @throws MemoryException
     */
    public function get(int $count = null, int $offset = 0)
    {
        if ($count == null) $count = $this->size() - $offset;
        if (($count + $offset) > $this->size()) {
            throw new MemoryException("Summary size out of memory", 97001);
        }
        return new Memory(StringByte::getBytes($this->memory, $count, $offset));
    }

    /**
     * Вырезает часть памяти которая будет возвращена
     *
     * @param int $count
     * @param int $offset
     * @param string $replace
     * @return Memory
     * @throws MemoryException
     */
    public function catOut(int $count, int $offset = 0, $replace = '')
    {
        $catMem = new Memory(substr($this->memory, $offset, $count));
        $this->memory = substr_replace($this->memory, $replace, $offset, $count);
        return $catMem;
    }

    /**
     * To string magic method
     *
     * @return string
     * @throws MemoryException
     */
    public function __toString()
    {
        return StringByte::printBytes($this->get()->toString(), true);
    }
}