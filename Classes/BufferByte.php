<?php

namespace Ponikrf\Indulib\Classes;

/**
 * Класс для сборки буффера байт
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class BufferByte
{
    protected $buffer = '';
    protected $bufferBlocks = [];

    public function init(string $buffer = '')
    {
        $this->buffer = $buffer;
        $this->bufferBlocks = [];
        return $this;
    }

    public function addBlockString(string $blockName, string $strBlock)
    {
        $this->bufferBlocks[$blockName] = $strBlock;
        return $this;
    }

    public function addBlockArray(string $blockName, array $arrayBlock)
    {
        $this->addBlockString($blockName, ArrayByteTo::string($arrayBlock));
        return $this;
    }

    public function addBlock(string $blockName, int $countByte, int $offset)
    {
        $this->bufferBlocks[$blockName] = StringByte::getBytes($this->buffer, $countByte, $offset);
        return $this;
    }

    public function addString(string $stringBytes, bool $ending = true)
    {
        $this->buffer = StringByte::catBytes($this->buffer, $stringBytes, $ending);
        return $this;
    }

    public function addArray(array $arrayBytes, bool $ending = true)
    {
        $this->buffer = StringByte::catBytes($this->buffer,  ArrayByteTo::string($arrayBytes), $ending);
        return $this;
    }

    public function addFunction(\Closure $closure){
        $this->buffer =  StringByte::catBytes($this->buffer,$closure($this));
        return $this;
    }

    public function addCRC16()
    {
        $this->buffer = StringByte::catBytes($this->buffer, CRC::CRC16($this->buffer));
        return $this;
    }

    public function toArray()
    {
        return StringByteTo::array($this->buffer);
    }

    public function toString()
    {
        return $this->buffer;
    }

    public function blocks()
    {
        return $this->bufferBlocks;
    }
}