<?php

namespace Ponikrf\Indulib\Classes;

/**
 * Класс для работы с битами. Упрощает понимание при чтении кода.
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Bin
{
    /**
     * Печатает (или возвращает) битовое представление числа
     *
     * @param $byte
     * @param int $bit
     * @return int
     */
    public static function print(int $number, bool $return = false)
    {
        if ($return) return '0b' . decbin($number);
        echo '0b' . decbin($number);
    }

    /**
     * Возвращает значение бита (первым битом считается нулевой бит)
     *
     * @param $byte
     * @param int $bit
     * @return int
     */
    public static function getBit($byte, int $bit = 0): int
    {
        return ($byte >> $bit) & 1;
    }

    /**
     * Возвращает маску бит заполненную единицами в количестве $bits
     *
     * @param int $bits
     * @return int
     */
    public static function bits(int $bits): int
    {
        return (1 << $bits) - 1;
    }

    /**
     * Устанавливает указанный бит в единицу (первым битом считается нулевой бит)
     *
     * @param $byte
     * @param int $bit
     * @return int
     */
    public static function setBit($byte, int $bit = 0): int
    {
        return $byte | (1 << $bit);
    }

    /**
     * Устанавливает указанный бит в ноль (первым битом считается нулевой бит)
     *
     * @param $byte
     * @param int $bit
     * @return int
     */
    public static function clearBit($byte, int $bit = 0): int
    {
        return $byte & (~(1 << $bit));
    }

    /**
     * Возвращает младший байт
     * @param $twoByte
     * @return int
     */
    public static function low($twoByte): int
    {
        return ($twoByte) & 0xff;
    }

    /**
     * Возвращает старший байт
     * @param $twoByte
     * @return int
     */
    public static function high($twoByte): int
    {
        return ($twoByte) >> 8;
    }

    /**
     * Считает количество битовых единиц
     *
     * @param int $number
     * @return int
     */
    public static function unitCount(int $number): int
    {
        for ($res = 0; $number; $res++) $number &= $number - 1;
        return $res;
    }
}
