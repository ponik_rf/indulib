<?php

namespace Ponikrf\Indulib\Classes;

/**
 * Класс приведения типов
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class DataType
{

    /**
     * Список доступных типов
     */
    const dataTypes = [
        'char', 'int8', 'uint8',
        'int16', 'uint16', 'uint16_be', 'uint16_le',
        'int32', 'uint32', 'uint32_be', 'uint32_le',
        'int64', 'uint64', 'uint64_be', 'uint64_le',
        'float', 'float_be', 'float_le',
        'double', 'double_be', 'double_le',
        'BCD8', 'BCD16', 'BCD24', 'BCD32', 'BCD48', 'BCD64',
        'CRC8', 'CRC16', 'CRC32',

    ];

    const dataTypesIndex = [
        'char' => 0, 'int8' => 1, 'uint8' => 2,
        'int16' => 3, 'uint16' => 4, 'uint16_be' => 5, 'uint16_le' => 6,
        'int32' => 7, 'uint32' => 8, 'uint32_be' => 9, 'uint32_le' => 10,
        'int64' => 11, 'uint64' => 12, 'uint64_be' => 13, 'uint64_le' => 14,
        'float' => 15, 'float_be' => 16, 'float_le' => 17,
        'double' => 18, 'double_be' => 19, 'double_le' => 20,
        'BCD8' => 21, 'BCD16' => 22, 'BCD24' => 23, 'BCD32' => 24, 'BCD48' => 25, 'BCD64' => 26,
        'CRC8' => 27, 'CRC16' => 28, 'CRC32' => 29,
    ];


    const packType = [
        'char' => 'C', 'int8' => 'c', 'uint8' => 'C',
        'int16' => 's', 'uint16' => 'S', 'uint16_be' => 'n', 'uint16_le' => 'v',
        'int32' => 'l', 'uint32' => 'L', 'uint32_be' => 'N', 'uint32_le' => 'V',
        'int64' => 'q', 'uint64' => 'Q', 'uint64_be' => 'J', 'uint64_le' => 'P',
        'float' => 'f', 'float_be' => 'G', 'float_le' => 'g',
        'double' => 'd', 'double_be' => 'E', 'double_le' => 'e',
    ];

    const BCDType = [
        'BCD8' => 1, 'BCD16' => 2, 'BCD24' => 3, 'BCD32' => 4, 'BCD48' => 6, 'BCD64' => 8,
    ];

    const CRCType = [
        'CRC8' => 1, 'CRC16' => 2, 'CRC32' => 4,
    ];

    /**
     * Размер данных в байтах
     */
    const dataTypesSize = [
        'char' => 1, 'int8' => 1, 'uint8' => 1,
        'int16' => 2, 'uint16' => 2, 'uint16_be' => 2, 'uint16_le' => 2,
        'int32' => 4, 'uint32' => 4, 'uint32_be' => 4, 'uint32_le' => 4,
        'int64' => 8, 'uint64' => 8, 'uint64_be' => 8, 'uint64_le' => 8,
        'float' => 4, 'float_be' => 4, 'float_le' => 4,
        'double' => 8, 'double_be' => 8, 'double_le' => 8,
        'BCD8' => 1, 'BCD16' => 2, 'BCD24' => 3,
        'BCD32' => 4, 'BCD48' => 6, 'BCD64' => 8,
        'CRC8' => 1, 'CRC16' => 2, 'CRC32' => 4,

    ];
}