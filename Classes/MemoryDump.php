<?php

namespace Ponikrf\Indulib\Classes;

/**
 * Класс для сборки буффера байт
 *
 * @property Memory $Memory
 * @property MemoryRule $Rule
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class MemoryDump
{

    protected $ModeSettings = [];
    protected $Memory;

    protected $xMax = 16;

    protected $startMods = [];
    protected $endMods = [];

    protected $selection = [];

    protected $scope = 1;

    protected $caption = false;

    /**
     * @param Memory $Memory
     * @param int $mode
     */
    public function __construct($Memory = "")
    {
        if ($Memory instanceof Memory){
            $this->setMemory($Memory);
        }

        $this->ModeSettings = [
            'nl' => "\n",
            'sHead' => '' . Console::start('cyan', false, 'cursive').' ',
            'eHead' => ' '.Console::end(),
            'empty' => ' ',
            'sAddr' => ' ' . Console::start('cyan', false, false),
            'eAddr' => '|' . Console::end(),
        ];
    }

    public function setCaption($caption){
        $this->caption = $caption;
        return $this;
    }

    public function fromString(string $string){
        $this->setMemory(new Memory($string));
        return $this;
    }

    /**
     * Назначаем память
     *
     *
     *
     * @param Memory $Memory
     * @return MemoryDump
     */
    public function setMemory(Memory $Memory)
    {
        $this->Memory = $Memory;
        return $this;
    }

    /**
     * Добавляет выделения
     *
     * @param $start
     * @param $count
     * @param $textColor
     * @param $background
     * @param $modify
     * @return MemoryDump
     */
    public function addSelection($start, $count, $textColor, $background, $modify)
    {
        $end = $start + $count;

        if ($this->Memory->size() < $end) {
            $end = $this->Memory->size();
        }

        for ($i = $start; $i < $end; $i++) {
            $this->selection[$i] = [
                'textColor' => $textColor,
                'background' => $background,
                'modify' => $modify,
                'start' => 0,
                'end' => 0,
            ];

            if ($i == $start) {
                $this->selection[$i]['start'] = 1;
            }

            if (($i + 1) == $end) {
                $this->selection[$i]['end'] = 1;
            }

        }
        return $this;
    }

    /**
     * Добавляет последовательные границы
     *
     * @param $count
     * @param bool $color
     * @param bool $modify
     * @return MemoryDump
     */
    public function addScope($count, $color = false, $modify = false)
    {
        $start = $this->scope;
        $end = $start + ($count - 1);

        if ($this->Memory->size() < $end) {
            $end = $this->Memory->size();
        }

        if (!array_key_exists($start, $this->startMods)) {
            $this->startMods[$start] = '';
        }

        if (!array_key_exists($end, $this->endMods)) {
            $this->endMods[$end] = '';
        }

        $this->startMods[$start] = Console::sprint("[", $color, false, 'bold') . $this->startMods[$start];
        $this->endMods[$end] .= Console::sprint("]", $color, false, 'bold');

        $this->scope += $count;

        return $this;
    }

    public function dump($return = false)
    {
        $result = PHP_EOL;

        if ($this->caption){
            $result .= Console::sprint("    ".$this->caption.":",false,false,'bold');
            $result .= PHP_EOL;
        }

        $addrChrCount = strlen(dechex($this->Memory->size()));

        $result .= Console::spice($addrChrCount + 2);

        // render header
        for ($i = 0; $i < $this->xMax; $i++) {
            $result .= $this->gs('sHead') . ArrayByte::printByte($i, true) . $this->gs('eHead');
        }

        $memoryString = $this->Memory->toString();

        $x = $this->xMax;
        for ($i = 0; $i < strlen($memoryString); $i++) {

            $byte = $i + 1;

            if ($x >= $this->xMax) {
                $x = 0;
                $result .= PHP_EOL;
                $result .= $this->gs('sAddr') . sprintf("%0{$addrChrCount}x", $i) . $this->gs('eAddr');
                #$result .= $this->gs('empty');
            }

            $start = $this->gs('empty');
            $end = $this->gs('empty');

            if (array_key_exists($byte, $this->startMods)) {
                $start = $this->startMods[$byte];
            }

            if (array_key_exists($byte, $this->endMods)) {
                $end = $this->endMods[$byte];
            }

            $printByte = StringByte::printByte($memoryString{$i}, true);

            if (array_key_exists($byte, $this->selection)) {

                $printByte = Console::sprint($printByte,
                    $this->selection[$byte]['textColor'],
                    $this->selection[$byte]['background'],
                    $this->selection[$byte]['modify']
                );

                if ($start == $this->gs('empty') AND $this->selection[$byte]['start'] != 1) {
                    $start = Console::sprint($start,
                        $this->selection[$byte]['textColor'],
                        $this->selection[$byte]['background'],
                        $this->selection[$byte]['modify']
                    );
                }
                if ($end == $this->gs('empty') AND $this->selection[$byte]['end'] != 1) {
                    $end = Console::sprint($end,
                        $this->selection[$byte]['textColor'],
                        $this->selection[$byte]['background'],
                        $this->selection[$byte]['modify']
                    );
                }

            }

            $result .= $start . $printByte . $end;
            $x++;
        }
        $result .= PHP_EOL;
        if ($return) {
            return $result;
        }
        echo $result;
    }

    /**
     * Возвращает настройку отображения
     *
     * @param $setting
     * @return mixed
     */
    protected function gs($setting)
    {
        return $this->ModeSettings[$setting];
    }
}