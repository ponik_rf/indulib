<?php

namespace Ponikrf\Indulib\Classes;

/**
 * Класс для работы с консольным выводом в linux
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Console
{
    const MODE_TEXT = 0;
    const MODE_LINUX = 1;
    const MODE_WINDOWS = 2;

    protected static $mode = 1;

    protected static $textColors = [
        'dark' => '30',
        'blue' => '34',
        'green' => '32',
        'cyan' => '36',
        'red' => '31',
        'purple' => '35',
        'brown' => '33',
        'yellow' => '33',
        'white' => '37',
    ];

    protected static $backgroundColors = [
        'black' => '40',
        'red' => '41',
        'green' => '42',
        'yellow' => '43',
        'blue' => '44',
        'magenta' => '45',
        'cyan' => '46',
        'light_gray' => '47',
    ];

    protected static $modifications = [
        'normal' => 0,
        'bold' => 1,
        'cursive' => 4,
        'flashing' => 5,
        'invert' => 7,
        'invisible' => 8,
    ];

    protected static function setMode($mode)
    {
        self::$mode = $mode;
    }

    /**
     * Печатает отформатированную строку
     *
     * @param $text
     * @param bool $textColor
     * @param bool $background
     * @param bool $modify
     * @param bool $return
     * @return string
     */
    public static function print($text, $textColor = false, $background = false, $modify = false, $return = false)
    {
        $start = self::start($textColor, $background, $modify);
        $end = self::end();

        if ($return) {
            return $start . $text . $end;
        }

        echo $start . $text . $end;
    }

    /**
     *
     * Начало форматированной строки
     *
     * @param bool $textColor
     * @param bool $background
     * @param bool $modify
     * @return string
     */
    public static function start($textColor = false, $background = false, $modify = false)
    {
        $start = "\x1b[";
        $modsArray = [];

        if ($modify AND $mod = self::getModify($modify)) {
            $modsArray[] = $mod;
        }

        if ($background AND $back = self::getBackground($background)) {
            $modsArray[] = $back;
        }

        if ($textColor AND $color = self::getTextColor($textColor)) {
            $modsArray[] = $color;
        }


        $start .= implode($modsArray, ';') . 'm';

        return $start;
    }
    /**
     * Окончание форматированной строки
     *
     * @return string
     */
    public static function end()
    {
        return "\x1b[0m";
    }

    /**
     * safe get
     *
     * @param $mod
     * @return bool|mixed
     */
    protected static function getModify($mod)
    {
        if (array_key_exists($mod, self::$modifications)) {
            return self::$modifications[$mod];
        }
        return false;
    }

    /**
     * safe get
     *
     * @param $color
     * @return bool|mixed
     */
    protected static function getTextColor($color)
    {
        if (array_key_exists($color, self::$textColors)) {
            return self::$textColors[$color];
        }
        return false;
    }

    /**
     * safe get
     *
     * @param $back
     * @return bool|mixed
     */
    protected static function getBackground($back)
    {
        if (array_key_exists($back, self::$backgroundColors)) {
            return self::$backgroundColors[$back];
        }
        return false;
    }


    /**
     * Печатает пробелы в количестве $count
     *
     * @param int $count
     * @return string
     */
    public static function spice(int $count)
    {
        for ($i = 0, $str = ''; $i < $count; $i++) $str .= ' ';
        return $str;
    }


    /**
     * Возвращает форматированный текст
     *
     * @param string $text
     * @param mixed $textColor
     * @param mixed $Background
     * @param mixed $modify
     * @return string
     */
    public static function sprint(string $text, $textColor=false, $Background=false, $modify=false)
    {
        return self::print($text, $textColor, $Background, $modify, true);
    }
}