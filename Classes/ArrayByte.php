<?php

namespace Ponikrf\Indulib\Classes;

use Ponikrf\Indulib\Exceptions\DataTypeException;

/**
 * Класс для работы с байтами, представленными в виде массива,
 * каждый элемент которого является байтом в виде числа **uint8_t**
 *
 * @author Boris Bobylev <ponik_rf@mail.com>
 */
class ArrayByte
{

    /**
     * Преобразует массив байт в указанный тип
     *
     * @param string $type
     * @param array $dataBytes
     * @return mixed
     * @throws DataTypeException
     */
    public static function cast(string $type, array $dataBytes)
    {
        $dataBytesArray = self::toString($dataBytes);
        return StringByte::cast($type, $dataBytesArray);
    }

    /**
     * Преобразует массив байт в массив данных указанного типа
     *
     * @param string $type
     * @param array $dataBytes
     * @return array
     * @throws \Exception
     */
    public static function castArray(string $type, array $dataBytes)
    {
        $dataBytesArray = self::toString($dataBytes);
        return StringByte::castArray($type, $dataBytesArray);
    }


    /**
     * Преобразует данные указанного типа в массив байт
     *
     * Рекомендуется использовать ArrayByteFrom
     *
     * @param string $type
     * @param $data
     * @return array
     * @throws DataTypeException
     */
    public static function pack(string $type, $data)
    {
        $sb = StringByte::pack($type, $data);
        return StringByte::toArray($sb);
    }


    /**
     * Преобразует массив данных в массив байт
     *
     * @param string $type
     * @param array $dataArray
     * @return array
     * @throws DataTypeException
     */
    public static function packArray(string $type, array $dataArray)
    {
        $sb = StringByte::packArray($type, $dataArray);
        return StringByte::toArray($sb);
    }

    /**
     * Печатает таблицу DEC HEX ASCII BIN для каждого байта
     *
     * @param array $dataBytes
     * @param bool $return
     * @return string|null
     */
    public static function dump(array $dataBytes, $return = false)
    {
        $out = '';
        $out .= sprintf("%6s %6s %6s %10s \n", 'DEC', 'HEX', "ASCII", "BIN");
        foreach ($dataBytes as $Byte) {
            $ASCII = array_key_exists($Byte, ASCII::ASCII_CODES) ? ASCII::ASCII_CODES[$Byte] : '';
            $out .= sprintf("%6d %6s %6s %10s \n", $Byte, dechex($Byte), $ASCII, decbin($Byte));
        }
        if ($return) return $out;
        echo $out;
    }

    /**
     * Возвращает массив байт в количестве $countBytes и пропустив от начала $offsetBytes
     *
     * @param array $dataBytes
     * @param int $countBytes
     * @param int $offsetBytes
     * @return array
     */
    public static function getBytes(array $dataBytes, int $countBytes = 1, int $offsetBytes = 0): array
    {
        $r = [];
        for ($i = $offsetBytes; $i < $offsetBytes + $countBytes; $i++) $r[] = $dataBytes[$i];
        return $r;
    }

    /**
     * Делит массив байт на части размером $countBytes в количестве $howMany
     * и возвращает результат в виде двумерного массива.
     *
     * @param array $dataBytes
     * @param int $countBytes
     * @param int $howMany
     * @return array
     */
    public static function getByteArray(array $dataBytes, int $countBytes = 1, int $howMany = 0): array
    {
        for ($i = 0, $r = []; $i < $howMany; $i++) $r[] = self::getBytes($dataBytes, $countBytes, $i * $countBytes);
        return $r;
    }

    /**
     * Генерирует массив случайных байт в количестве $Bytes
     *
     * @param int $Bytes
     * @return array
     */
    public static function randomBytes(int $Bytes): array
    {
        $bytesArray = [];
        for ($i = 0; $i < $Bytes; $i++) $bytesArray[] = rand(0, 255);
        return $bytesArray;
    }

    /**
     * Пересоздает массив по порядку и перестраивает индексы от 0
     *
     * @param array $dataBytes
     * @return array
     */
    public static function remake(array $dataBytes): array
    {
        $result = [];
        foreach ($dataBytes as $byte) $result[] = $byte;
        return $result;
    }

    /**
     * Преобразует 2 массива байт в один с порядком $ending
     *
     * @param array $a
     * @param array $b
     * @param bool $ending
     * @return array
     */
    public static function catBytes(array $a, array $b, bool $ending = true): array
    {
        if ($ending) return array_merge($a, $b);
        return array_merge($b, $a);
    }


    /**
     * Возвращает количество байт
     *
     * @param array $arrayBytes
     * @return int
     */
    public static function size(array $arrayBytes): int
    {
        return count($arrayBytes);
    }

    /**
     * Возвращает обратный порядок байт, группируя блоки байт размером $blockSize
     *
     * @param array $dataBytes
     * @param int $blockSize
     * @return array
     */
    public static function reverseBytes(array $dataBytes, $blockSize = 1): array
    {
        if ($blockSize == 1) return array_reverse($dataBytes);
        $dataArray = self::getByteArray($dataBytes, $blockSize, (int) (self::size($dataBytes) / $blockSize));
        $res = [];
        foreach ($dataArray as $data) $res = self::catBytes($res, $data, false);
        return $res;
    }

    /**
     * Преобразует форматированную строку в массив байт
     *
     * @param $strBytes
     * @param string $delimiter
     * @return array|number
     * @internal param $stringByte
     */
    public static function fromFormatString($strBytes, $delimiter = ':')
    {
        $r = [];
        $strArray = explode($delimiter, $strBytes);
        foreach ($strArray as $byte) $r[] = hexdec($byte);
        return $r;
    }

    /**
     * Печатает строку байт
     *
     * @param array $byteArray
     * @param bool $return
     * @param int $size
     * @return string
     */
    public static function printBytes(array $byteArray, $return = false, $size = 0)
    {
        $result = '';
        $i = 0;
        foreach ($byteArray as $byte) {
            if ($size > 0 and $i >= $size) {
                $result .= "...(" . (count($byteArray) - $i) . ")";
                break;
            }
            if ($i > 0) $result .= ':' . self::printByte($byte, true);
            else  $result .= self::printByte($byte, true);
            $i++;
        }
        if ($return) return $result;
        echo $result;
    }

    /**
     * Печатает (или возвращает) HEX символ байта
     *
     * @param integer $byte
     * @param bool $return
     * @return string
     */
    public static function printByte(int $byte, $return = false)
    {
        $char = sprintf("%02X", $byte);
        if ($return) return $char;
        echo $char;
    }

    /**
     * Возвращает true если считает, что с точки зрения PHP 
     * данное число может преобразоватся в байт
     *
     * @param int $byte
     * @return bool
     */
    public static function isByte(int $byte)
    {
        if ($byte > 255 or $byte < 0) return false;
        return true;
    }

    /**
     * Конвертирует массив байт в строку
     *
     * @param array $bytesArray
     * @return string
     */
    public static function toString(array $bytesArray = []): string
    {
        try {
            $result = StringByte::packArray('uint8', $bytesArray);
        } catch (DataTypeException $e) {
            $result = '';
        }
        return $result;
    }
}
