<?php

namespace Ponikrf\Indulib\Classes;

/**
 * Класс для работы с ASCII символами
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class ASCII
{
    const ASCII_INVISIBLE_CODES = [
        'NUL', 'SOH', 'STX', 'ETX', 'EOT', 'ENQ', 'ACK', 'BEL', 'BS', 'HT', 'LF', 'VT', 'FF', 'CR', 'SO', 'SI',
        'DLE', 'DC1', 'DC2', 'DC3', 'DC4', 'NAK', 'SYN', 'ETB', 'CAN', 'EM', 'SUB', 'ESC', 'FS', 'GS', 'RS', 'US',
        'SPC',
        0x7F => 'DEL',
    ];

    const ASCII_CODES = [
        'NUL', 'SOH', 'STX', 'ETX', 'EOT', 'ENQ', 'ACK', 'BEL', 'BS', 'HT', 'LF', 'VT', 'FF', 'CR', 'SO', 'SI',
        'DLE', 'DC1', 'DC2', 'DC3', 'DC4', 'NAK', 'SYN', 'ETB', 'CAN', 'EM', 'SUB', 'ESC', 'FS', 'GS', 'RS', 'US',
        'SPC', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/',
        '0', '1', '2', '3', '4', '5', '6', "7", '8', '9', ':', ';', '<', '=', '>', '?',
        '@', 'A', 'B', 'C', 'D', 'E', 'F', "G", 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
        'P', 'Q', 'R', 'S', 'T', 'U', 'V', "W", 'X', 'Y', 'Z', '[', '\\', ']', '^', '_',
        '`', 'a', 'b', 'c', 'd', 'e', 'f', "g", 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
        'p', 'q', 'r', 's', 't', 'u', 'v', "w", 'x', 'y', 'z', '{', '|', '}', '~', 'DEL',
    ];


    public static $DEFAULT_PRINT_OPTIONS = [
        'start_nc' => '',  // Начало нормального символа
        'end_nc' => '',    // Окончание нормального символа
        'start_cc' => '',  // Начало управляющего символа
        'end_cc' => '',    // Конец управляющего символа
        'use_CR' => true,   // Печатает символ перевода строки вместе с абривиатурой
        'return' => false,  // возврат результата вместо печати
    ];

    /**
     * Приводит строку или массив байт к ASCII формату.
     * Позволяет быстро убрать бит четности для корректного отображения текста.
     * Производит модифицирование непосредственно переданого значения.
     * Возвращает `true` в случае успеха.
     *
     * @param mixed $mixed строка или массив байт
     * @return bool
     */
    public static function format(&$mixed)
    {
        if (is_string($mixed)) {
            $array = ArrayByteFrom::string($mixed);
            if (self::formatArray($array))
                $mixed = StringByteFrom::array($array);
            else
                return false;
            return true;
        }
        if (is_array($mixed)) return self::formatArray($mixed);
    }

    /**
     * Добавляет к строке или массиву байт бит четности.
     * Производит модифицирование непосредственно переданого значения.
     * Возвращает `true` в случае успеха.
     *
     * @param $mixed
     * @return bool
     */
    public static function addControlBit(&$mixed)
    {
        if (is_string($mixed)) {
            $array = ArrayByteFrom::string($mixed);
            if (self::controlBitArray($array))
                $mixed = StringByteFrom::array($array);
            else
                return false;
            return true;
        }
        if (is_array($mixed)) return self::controlBitArray($mixed);
    }

    /**
     * Печатает ASCII строку заменяя несуществущие символы их определением.
     *
     * @param $mixed
     * @param bool $return
     * @return string
     */
    public static function printBytes($mixed, $options = [])
    {
        $options = array_merge(self::$DEFAULT_PRINT_OPTIONS, $options);
        if (is_string($mixed)) $mixed = ArrayByteFrom::string($mixed);
        $result = '';

        if (is_array($mixed)) {
            self::formatArray($mixed);
            foreach ($mixed as $value) {
                $result .= self::printByte($options, $value);
            }
        }
        if ($options['return']) return $result;
        echo $result . PHP_EOL;
    }

    /**
     * форматирует для печати/возврата байт
     *
     * @param array $options
     * @param int $byte
     * @return string
     */
    protected static function printByte(array &$options, int $byte)
    {
        $char = self::ASCII_CODES[$byte];
        if (array_key_exists($byte, self::ASCII_INVISIBLE_CODES)) {
            $end = '';
            if ($byte == 0x0A and $options['use_CR']) $end = PHP_EOL;
            return $options['start_cc'] . $char . $options['end_cc'] . $end;
        } else {
            return $options['start_nc'] . $char . $options['end_nc'];
        }
    }

    /**
     * Приводит массив байт к ASCII формату
     *
     * @param array $controlBitArray
     * @return bool
     */
    protected static function controlBitArray(array &$controlBitArray)
    {
        foreach ($controlBitArray as &$controlBitValue)
            if (Bin::unitCount($controlBitValue) & 1) $controlBitValue |= 0x80;
        return true;
    }

    /**
     * Приводит массив байт к ASCII формату
     *
     * @param array $toFormatArray
     * @return bool
     */
    protected static function formatArray(array &$toFormatArray)
    {
        foreach ($toFormatArray as &$value) $value &= 0x7F;
        return true;
    }
}
