<?php

namespace Ponikrf\Indulib\Classes;
use Ponikrf\Indulib\Exceptions\MemoryException;
use Ponikrf\Indulib\Exceptions\PackException;

/**
 * Класс для сборки буффера байт
 *
 * @property Memory $Memory
 * @property Rule $Rule
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Pack
{
    /** CLASSES */
    protected $Memory;
    protected $Rule;

    /** Status Pack */
    const STATUS_NO_PACK = 0;
    const STATUS_ERROR = -1;
    const STATUS_SUCCESS = 1;

    const STATUS = [
        0 => 'NO PACK',
        -1 => 'ERROR',
        1 => 'SUCCESS',
    ];

    protected $status = self::STATUS_NO_PACK;

    /** tmp vars */
    protected $render = '';
    protected $storage = [];
    protected $ruleArray = [];
    protected $error = null;

    /**
     * @param Rule $Rule
     * @param bool $autoPack
     * @throws MemoryException
     */
    public function __construct(Rule $Rule, $autoPack = false)
    {
        $this->Memory = new Memory();
        $this->Rule = $Rule;
        if ($autoPack) $this->pack();
    }


    /**
     * Возвращает строку результата (magic)
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * Возвращает строку результата
    */
    public function toString(){
        return $this->Memory->toString();
    }

    public function dump($return = false){
        $result = 'STATUS: '.self::STATUS[$this->status].PHP_EOL;
        $result .= 'MEMORY: '.(string)$this->Memory.PHP_EOL;
        $result .= 'RESULT: '.print_r($this->render,true);
        if($return) return $result;
        echo $result;
    }

    public function pack()
    {
        if ($this->status != self::STATUS_NO_PACK) {
            if ($this->status == self::STATUS_SUCCESS) return true;
            return false;
        }
        $this->index = 0;
        $this->ruleArray = [
            'type' => "Rule",
            'name' => "root",
            'count' => 1,
            'regions' => $this->Rule->toArray(),
        ];

        try {
            $this->renderRule($this->ruleArray, $this->render);
            $this->status = self::STATUS_SUCCESS;
            $this->Memory->set($this->render);
            return true;
        } catch (\Exception $exception) {
            $this->error = $exception;
        }
        $this->Memory->set($this->render);
        $this->status = self::STATUS_ERROR;
        return false;
    }

    /**
     *
     * Рекурсивная функция обработки правила
     *
     * @param $Rule
     * @param $render
     * @return bool
     * @throws PackException
     * @throws \Ponikrf\Indulib\Exceptions\DataTypeException
     */
    public function renderRule(&$Rule, &$render)
    {
        foreach ($Rule['regions'] AS &$item) {
            switch ($item['type']) {
                case 'Rule':
                    $this->renderRule($item, $render);
                    break;
                case 'Field':
                    if (array_key_exists('count', $item)) {
                        foreach ($item['regions'] AS $one) {
                            $this->renderField($one, $render);
                        }
                    } else {
                        $this->renderField($item, $render);
                    }
                    break;
            }
        }
        return true;
    }

    /**
     *
     * Обработка поля
     *
     * @param $Field
     * @param $render
     * @return bool
     * @throws PackException
     * @throws \Ponikrf\Indulib\Exceptions\DataTypeException
     */
    public function renderField(&$Field, &$render)
    {
        if ($Field['regions']['type'] == 'void') {
            for ($i = 0, $zero = []; $i < $Field['regions']['size']; $i++) $zero[] = 0;
            $Field['regions']['bytes'] = StringByteFrom::array($zero);
            $this->fieldAddToStorage($Field);
            $render = StringByte::catBytes($render, $Field['regions']['bytes']);
            return true;
        }

        if ($Field['regions']['type'] == 'data') {
            if (!is_string($Field['value'])) {
                throw PackException::create(3104);
            }
            $Field['regions']['bytes'] = $Field['value'];
            $this->fieldAddToStorage($Field);
            $render = StringByte::catBytes($render, $Field['value']);
            return true;
        }

        if (array_key_exists($Field['regions']['type'], DataType::CRCType)) {
            $Field['regions']['bytes'] = CRC::crc($Field['regions']['type'], $this->storage[$Field['name']]);
            $this->fieldAddToStorage($Field);
            $render = StringByte::catBytes($render, $Field['regions']['bytes']);
            return true;
        }

        if (in_array($Field['regions']['type'], DataType::dataTypes)) {

            try {
                $Field['regions']['bytes'] = StringByte::pack($Field['regions']['type'], $Field['value']);
            } catch (\Exception $e) {
                throw PackException::create(3101);
            }

            if (!strlen($Field['regions']['bytes'])) throw PackException::create(3102);
            if (strlen($Field['regions']['bytes']) != $Field['regions']['size']) throw PackException::create(3103);

            $this->fieldAddToStorage($Field);
            $render = StringByte::catBytes($render, $Field['regions']['bytes']);
            return true;
        }
    }


    /**
     * Добавляет данные в локальное хранилище
     *
     * @param $Field
     */
    protected function fieldAddToStorage(&$Field)
    {
        foreach ($Field['regions']['store'] AS $toStore) {
            if (!array_key_exists($toStore['store'], $this->storage)) $this->storage[$toStore['store']] = '';
            $this->storage[$toStore['store']] = StringByte::catBytes(
                $this->storage[$toStore['store']],
                $Field['regions']['bytes'],
                $toStore['ending']
            );
        }
    }
    /**
     * Возвращет статус преобразования
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Получить результат приведения данных
     *
     * @return string
     */
    public function result()
    {
        return $this->Memory;
    }

    public function getError()
    {
        return $this->error;
    }

    /**
     * Получить результат приведения данных
     *
     * @return array
     */
    public function ruleArray()
    {
        return $this->ruleArray;
    }
}