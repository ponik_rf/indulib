<?php

namespace Ponikrf\Indulib\Providers;

use Ponikrf\Indulib\Classes\StringByte;
use Ponikrf\Indulib\Exceptions\ProviderException;

/**
 *
 * Провайдер через сокет
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
Class SocketProvider implements ProviderInterface
{

    /** Ссылка на сокет */
    protected $socket;

    /** Модель */
    protected $Model = false;

    /** Адрес подключения */
    protected $address = '';

    /** Debug mode */
    protected $debug = false;

    /** Timeout на подключение */
    protected $connectionTimeout = 2;

    /** Timeout на чтение */
    protected $readTimeout = 3;

    /** Timeout на чтение в микросекундах */
    protected $readTimeoutMicroseconds = 0;

    /** Определяет блокирующий сокет */
    protected $blocking = false;

    /** буфер чтения */
    protected $readBuffer = '';

    protected $status = false;

    public function connect()
    {
        if ($this->debug) echo "Try connect: " . $this->address . PHP_EOL;

        if (($this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) < 0) {
            throw (new ProviderException("Не удалось создать сокет", 11001))
                ->setModel($this->Model);
        }

        socket_set_nonblock($this->socket);
        $attempts = 0;

        $tmp = explode(':',$this->address);

        while (!($connected = @socket_connect($this->socket, $tmp[0], $tmp[1]+0)) && $attempts++ < $this->connectionTimeout) {
            if ($this->debug) echo "Check connect... ". PHP_EOL;
            $error = socket_last_error();
            if ($error != SOCKET_EINPROGRESS && $error != SOCKET_EALREADY) {
                socket_close($this->socket);
                $this->status = false;
                throw (new ProviderException("Не удалось подключится к провайдеру", 11001))
                    ->setModel($this->Model)
                    ->setData(socket_strerror(socket_last_error()));
            }
            sleep(1);
        }

        if (!$connected){
            $this->status = false;
            socket_close($this->socket);
            throw (new ProviderException("Не удалось подключится к провайдеру", 11001))
                ->setModel($this->Model)
                ->setData(socket_strerror(socket_last_error()));

        }

        if ($this->debug) echo "Success connect:" . $this->address . PHP_EOL;
        $this->status = true;
        socket_set_block($this->socket);
    }

    public function disconnect()
    {
        try {
            if ($this->debug) echo "Try disconnect. " . PHP_EOL;
            socket_close($this->socket);
            if ($this->debug) echo "Sleep one second. " . PHP_EOL;
            $this->status = false;
            sleep(1);
        } catch (\Exception $exception) {
            throw (new ProviderException("Не удалось закрыть соединение", 11006))
                ->setModel($this->Model)
                ->setData([
                    'message' => $exception->getMessage(),
                    'code' => $exception->getCode(),
                ]);
        }
    }

    public function setModel($Model)
    {
        $this->Model = $Model;
    }

    public function getModel()
    {
        return $this->Model;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getStatus()
    {
        if (!$this->socket) {
            $this->status = false;
        }
        return $this->status;
    }


    public function write(string $str)
    {
        if ($this->debug) echo "Write to socket: " . StringByte::printBytes($str, true);

        $result = socket_write($this->socket, $str,strlen($str));

        if ($result === false || $result === null || $result === 0) {
            throw (new ProviderException("Проблемы отправки данных", 11003))
                ->setModel($this->Model);
        }

        return true;
    }

    public function read(int $len = 1024, int $timeout = 1, int $microsecond = 0)
    {
        if ($timeout OR $microsecond) $this->setReadTimeout($timeout, $microsecond);

        try {

            if ($this->debug) echo "Try read byte from socket: ";
            $strBytes = socket_read($this->socket, $len);

            if (empty($strBytes)) {
                return 0;
            }

            if ($this->debug) echo StringByte::printBytes($strBytes, true);

            $this->readBuffer = StringByte::catBytes($this->readBuffer, $strBytes);
            return strlen($strBytes);

        } catch (\Exception $exception) {
            throw (new ProviderException("Проблемы получения данных", 11004))
                ->setModel($this->Model)
                ->setData([
                    'message' => $exception->getMessage(),
                    'code' => $exception->getCode(),
                ]);
        }
    }

    public function request(string $dataBytes, int $timeout = 1, int $microsecond = 0, int $count, \Closure $checkPackage)
    {
        if (!$this->getStatus()) {
            $this->connect();
        }
        $this->clean(0, 200000);
        try {
            $this->write($dataBytes);
        } catch (ProviderException $exception) {
            $this->connect();
            $this->write($dataBytes);
        }
        $this->setReadBuffer('');
        $errors = 0;
        while ($errors < $count) {
            if ($this->read(1024, $timeout, $microsecond)) {
                if ($checkPackage($this->getReadBuffer())) {
                    return true;
                }
            }
            $errors++;
        }
        return false;
    }

    public function clean(int $timeoutSecond, int $timeoutMicrosecond)
    {
        while (1) {
            if ($this->read(1024, $timeoutSecond, $timeoutMicrosecond)) {
                continue;
            }
            break;
        }

        $this->setReadBuffer('');
        return true;
    }

    /**
     * Set socket timeout read for blocking mode
     *
     * @param int $second
     * @param $microsecond
     * @return void
     * @throws ProviderException
     */
    protected function setReadTimeout(int $second, $microsecond)
    {
        try {
            if ($this->debug) echo "Read timeout: $second, $microsecond \n";
            socket_set_option($this->socket,SOL_SOCKET, SO_RCVTIMEO, ["sec"=>$second, "usec"=>$microsecond]);
        } catch (\Exception $exception) {
            throw (new ProviderException("Проблема установки timeout чтения", 11007))
                ->setModel($this->Model)
                ->setData([
                    'message' => $exception->getMessage(),
                    'code' => $exception->getCode(),
                ]);
        }
    }

    public function setReadBuffer(string $buffer)
    {
        $this->readBuffer = $buffer;
    }

    public function getReadBuffer(): string
    {
        return $this->readBuffer;
    }

    public function setDebug(bool $debug)
    {
        $this->debug = $debug;
        return $this;
    }

}

