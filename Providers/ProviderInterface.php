<?php

namespace Ponikrf\Indulib\Providers;

use Ponikrf\Indulib\Exceptions\ProtocolException;
use Ponikrf\Indulib\Exceptions\ProviderException;

/**
 * Интерфейс провайдера
 * Каждый провайдер должен реализовывать этот интерфейс
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
*/
interface ProviderInterface
{

    /**
     * Проводит попытку соединения c провайдером
     * В случае неудачи генерирует исключение с ошибкой 11001
     *
     * возвращает всегда true
     *
     * @throws ProviderException
     * @return bool
    */
    public function connect();

    /**
     * Пытается разорвать связь с провайдером
     * В случае неудачи генерирует исключение с ошибкой 11006
     *
     * @throws ProviderException
     * @return bool
     */
    public function disconnect();

    /**
     * Назначает модель которая считается владельцем
     * данного конкретного провайдера
     *
     * @param $Model
     * @return bool
     */
    public function setModel($Model);

    /**
     * Возвращает модель владельца
     *
     * @see setModel();
     * @return mixed
    */
    public function getModel();

    /**
     * Устанавливает адрес назначения првайдера
     *
     * @param $address
     * @return bool
     */
    public function setAddress($address);

    /**
     * Читает  $countBytes байт складывает их в readBuffer
     * который должен быть доступен через getReadBuffer
     * В случае неудачи генерирует исключение с ошибкой 11004
     *
     * Возвращает количество байт полученых при чтении
     *
     * @throws ProviderException
     * @param int $countBytes
     * @param int $timeoutSecond
     * @param int $timeoutMicrosecond
     * @return int
     */
    public function read(int $countBytes, int $timeoutSecond, int $timeoutMicrosecond);

    /**
     * Отправляет строку провайдеру
     *
     * В случае неудачи генерирует исключение с ошибкой 11003
     *
     * @throws ProviderException
     * @param string $dataBytes
     * @return true
     */
    public function write(string $dataBytes);

    /**
     * Метод проводит подготовительные работы и отправляет запрос
     * пытаясь прочитать данные от провайдера с таймаутом $timeout
     * в количестве $count
     *
     * Что бы сократить время на запросы чтения, необходимо передать
     * функцию $checkPackage которая должна вернуть true если считает что
     * целый пакет был получен
     *
     * Наприме:
     *
     * function ($buffer){
     *
     *      if ($buffer > 10){
     *          return true;
     *      }
     *      false;
     * }
     *
     * Метод возвращает true если было получено подтверждение что пакет был получен полностью
     * в противном случае метод вернет false
     *
     * @param string $dataBytes
     * @param int $timeout
     * @param int $microsecond
     * @param int $count количество попыток отправки
     * @param \Closure $checkPackage
     * @throws ProtocolException|ProviderException
     *
     * @return bool
     */
    public function request(string $dataBytes,int $timeout = 1, int $microsecond = 0, int $count, \Closure $checkPackage);

    /**
     * Очищает провайдер
     *
     * Пытается прочитать из провайдера все данные с таймаутами
     * Если удалось прочитать какие либо данные, производится попытка
     * произвести повторное чтение пока данные провайдера не кончатся
     *
     * После чего производится очищение буфера чтения
     *
     * @throws ProviderException
     * @param int $timeoutSecond
     * @param int $timeoutMicrosecond
     * @return true
     */
    public function clean(int $timeoutSecond, int $timeoutMicrosecond);

    /**
     * Включает/выключает debug режим
     *
     * Всегда возвращает true
     *
     * @param bool $debug
     * @return bool
     */
    public function setDebug(bool $debug);

    /**
     * Возвращает статус провайдера
     *
     * @return bool
    */
    public function getStatus();

    /**
     * Возвращает буфер чтения
     *
     * @return string
    */
    public function getReadBuffer();

    /**
     * Устанавливает буфер чтения
     *
     * @param string $dataBytes
     * @return bool
     */
    public function setReadBuffer(string $dataBytes);
}