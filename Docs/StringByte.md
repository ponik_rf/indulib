StringByte
=============

Класс для работы с байтами, представленными в виде строки.

Методы:

 * [cast](#cast) - Преобразует строку байт в указанный тип.
 * [castArray](#castarray) - Преобразует строку байт в массив данных указанного типа.
 * [pack](#pack) - Преобразует данные указанного типа в строку байт.
 * [packArray](#packarray) - Преобразует массив данных указанного типа в строку байт.
 * [dump](#dump) - Печатает таблицу DEC HEX ASCII BIN для каждого байта.
 * [getBytes](#getbytes) - Возвращает строку байт в количестве $countBytes и пропустив от начала $offsetBytes.
 * [getByteArray](#getbytearray) - Делит строку байт на части размером $countBytes в количестве $howMany и возвращает результат в виде массива строк.
 * [randomBytes](#randombytes) - Генерирует строку случайных байт в количестве $Bytes.
 * [catBytes](#catbytes) - Преобразует 2 строки байт в одну с порядком $ending
 * [size](#size) - Возвращает количество байт
 * [reverseBytes](#reversebytes) - Возвращает обратный порядок байт, группируя блоки байт размером $blockSize
 * [fromFormatString](#fromformatstring) - Преобразует форматированную строку в строку байт используя в качестве разделителя $delimiter
 * [printBytes](#printbytes) - Печатает строку байт (или возвращает) ограниченную размером $size байт
 * [printByte](#printbyte) - Печатает (или возвращает) HEX символ байта
 * [isByte](#isbyte) - Возвращает true если считает что с точки зрения PHP данная строка может преобразоватся в байт
 * [toArray](#toarray) - Конвертирует строку байт в массив


## cast
Преобразует строку байт в указанный тип.
```php
public static function cast(string $type, string $dataBytes)
```
Размер строки должен соответствовать размеру типа данных.
Рекомендуется использовать `Ponikrf\Indulib\Classes\StringByteTo`.
Посмотреть поддерживаемые типы можно в файле `Ponikrf\Indulib\Classes\DataType`.
Если тип не найден - бросит исключение `DataTypeException`.
```php
$stringBytes = StringByte::fromFormatString('01:02');
echo StringByte::cast('uint16_be', $stringBytes);
```
```
258
```

## castArray
Преобразует строку байт в массив данных указанного типа.
```php
public static function castArray(string $type, string $dataBytes)
```
Используется для преобразования данных, идущих подряд в массив указанного типа.
Необходимо, что бы память была кратной указанному размеру.

Посмотреть поддерживаемые типы можно в файле `Ponikrf\Indulib\Classes\DataType`.
Если тип не найден - бросит исключение `DataTypeException`.
```php
$stringBytes = StringByte::fromFormatString('01:02:01:03');
$result = StringByte::castArray('uint16_be', $stringBytes);
print_r($result);
```
```
Array
(
    [0] => 258
    [1] => 259
)
```

## pack
Преобразует данные указанного типа в строку байт.
```php
public static function pack(string $type, $data): string
```
Рекомендуется использовать `Ponikrf\Indulib\Classes\ArrayByteFrom`.
Посмотреть поддерживаемые типы можно в файле `Ponikrf\Indulib\Classes\DataType`.
Если тип не найден - бросит исключение `DataTypeException`.
```php
$stringBytes = StringByte::pack('uint16_be', 258);
StringByte::printBytes($stringBytes);
```
```
01:02
```

## packArray
Преобразует массив данных в массив байт указанного типа.
```php
public static function packArray(string $type, array $dataArray): string
```
Посмотреть поддерживаемые типы можно в файле `Ponikrf\Indulib\Classes\DataType`.
Если тип не найден - бросит исключение `DataTypeException`.
```php
$stringBytes = StringByte::packArray('uint16_be', [258, 259]);
StringByte::printBytes($stringBytes);
```
```
01:02:01:03
```

## dump
Печатает таблицу DEC HEX ASCII BIN для каждого байта
```php
public static function dump(string $dataBytes, $return = false)
```
Если указать `$return = true` результат будет возвращен.
```php
$stringBytes = StringByte::fromFormatString('01:02:03:04');
StringByte::dump($stringBytes);
```
```
   DEC    HEX  ASCII        BIN
     1      1    SOH          1
     2      2    STX         10
     3      3    ETX         11
     4      4    EOT        100
```

## getBytes
Возвращает строку байт в количестве $countBytes и пропустив от начала $offsetBytes.
```php
public static function getBytes(string $dataBytes, int $countBytes = 1, int $offsetBytes = 0): string
```
```php
$stringBytes = StringByte::fromFormatString('01:02:03:04');
$result = StringByte::getBytes($stringBytes, 2, 2);
StringByte::printBytes($result);
```
```
03:04
```

## getByteArray
Делит строку байт на части размером $countBytes в количестве $howMany и возвращает результат в виде массива строк.
```php
public static function getByteArray(string $dataBytes, int $countBytes = 1, int $howMany = 0): array
```
```php
$stringBytes = StringByte::fromFormatString('01:02:03:04');
$result = StringByte::getByteArray($stringBytes, 2, 2);
array_walk($result, function($item){
    StringByte::printBytes($item);
});
```
```
Array
01:02
03:04
```


## randomBytes
Генерирует строку случайных байт в количестве $Bytes.
```php
public static function randomBytes(int $Bytes): string
```
```php
$stringBytes = StringByte::randomBytes(4);
StringByte::printBytes($stringBytes);
```
Получаем всегда разные значения
```
41:CB:5C:A5
```

## catBytes
Преобразует 2 строки байт в одну с порядком $ending
```php
public static function catBytes(string $a, string $b, bool $ending = true): string
```
```php
$a = StringByte::fromFormatString('01:02');
$b = StringByte::fromFormatString('03:04');
$stringBytes = StringByte::catBytes($a,$b);
StringByte::printBytes($stringBytes);
```
```
01:02:03:04
```

## size
Возвращает количество байт
```php
public static function size(string $dataBytes): int
```
```php
$stringBytes = StringByte::fromFormatString('01:02:03:04');
echo StringByte::size($stringBytes);
```
```
4
```

## reverseBytes
Возвращает обратный порядок байт, группируя блоки байт размером $blockSize
```php
public static function reverseBytes(string $dataBytes, int $blockSize = 1): string
```
```php
$stringBytes = StringByte::fromFormatString('01:02:03:04:05:06:07:08');
$result = StringByte::reverseBytes($stringBytes, 1);
StringByte::printBytes($result);
```
```
08:07:06:05:04:03:02:01
```
Группировка по 2 байта
```php
$stringBytes = StringByte::fromFormatString('01:02:03:04:05:06:07:08');
$result = StringByte::reverseBytes($stringBytes, 2);
StringByte::printBytes($result);
```
```
07:08:05:06:03:04:01:02
```
Группировка по 4 байта
```php
$stringBytes = StringByte::fromFormatString('01:02:03:04:05:06:07:08');
$result = StringByte::reverseBytes($stringBytes, 4);
StringByte::printBytes($result);
```
```
05:06:07:08:01:02:03:04
```


## fromFormatString
 Преобразует форматированную строку в строку байт используя в качестве разделителя $delimiter
```php
public static function fromFormatString(string $strBytes, string $delimiter = ':'): string
```
```php
$stringBytes = StringByte::fromFormatString('01 02 03 04',' ');
StringByte::printBytes($stringBytes);
```
```
01:02:03:04
```


## printBytes
Печатает строку байт (или возвращает) ограниченную размером $size байт
```php
public static function printBytes(string $byteString, bool $return = false, int $size = 0)
```
Можно вернуть результат используюя `$return = true`. Так же можно ограничить количество печатаемых байт указав размер $size.
```php
$stringBytes = StringByte::fromFormatString('01:02:03:04:05:06:07:08:09');
StringByte::printBytes($stringBytes,false,4);
```
```
01:02:03:04...(5)
```


## printByte
Печатает (или возвращает) HEX символ байта
```php
public static function printByte(string $byte, bool $return = false): string
```
Можно вернуть результат используюя `$return = true`.


## isByte
Возвращает true если считает, что с точки зрения PHP данное число может преобразоватся в байт
```php
public static function isByte(string $byte): bool
```


## toArray
Конвертирует строку в массив байт
```php
public static function toArray(string $dataBytes): array
```
Возвращает `[]` в случае если массив не может быть преобразован.
```php
$stringBytes = StringByte::fromFormatString('01:02:03:04');
$arrayBytes = StringByte::toArray($stringBytes);
ArrayByte::printBytes($arrayBytes);
```
```
01:02:03:04
```
