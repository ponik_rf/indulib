ArrayByte
=============

Класс для работы с байтами, представленными в виде массива,
каждый элемент которого является байтом в виде числа **uint8_t**.

Методы:

 * [cast](#cast) - Преобразует массив байт в указанный тип.
 * [castArray](#castarray) - Преобразует массив байт в массив данных указанного типа.
 * [pack](#pack) - Преобразует данные указанного типа в массив байт.
 * [packArray](#packarray) - Преобразует массив данных в массив байт указанного типа.
 * [dump](#dump) - Печатает таблицу DEC HEX ASCII BIN для каждого байта.
 * [getBytes](#getbytes) - Возвращает массив байт в количестве $countBytes и пропустив от начала $offsetBytes.
 * [getByteArray](#getbytearray) - Делит массив байт на части размером $countBytes в количестве $howMany и возвращает результат в виде двумерного массива.
 * [randomBytes](#randombytes) - Генерирует массив случайных байт в количестве $Bytes.
 * [remake](#remake) - Пересоздает массив по порядку и перестраивает индексы от 0
 * [catBytes](#catbytes) - Преобразует 2 массива байт в один с порядком $ending
 * [size](#size) - Возвращает количество байт
 * [reverseBytes](#reversebytes) - Возвращает обратный порядок байт, группируя блоки байт размером $blockSize
 * [fromFormatString](#fromformatstring) - Преобразует форматированную строку в массив байт используя разделитель $delimiter
 * [printBytes](#printbytes) - Печатает строку байт (или возвращает) ограниченную размером $size байт
 * [printByte](#printbyte) - Печатает (или возвращает) HEX символ байта
 * [isByte](#isbyte) - Возвращает true если считает, что с точки зрения PHP данное число может преобразоватся в байт
 * [toString](#tostring) - Конвертирует массив байт в строку


## cast
Преобразует массив байт в указанный тип.
```php
public static function cast(string $type, array $dataByte);
```
Размер массива должен соответствовать размеру данных.
Рекомендуется использовать `Ponikrf\Indulib\Classes\ArrayByteTo`.
Посмотреть поддерживаемые типы можно в файле `Ponikrf\Indulib\Classes\DataType`.
Если тип не найден - бросит исключение `DataTypeException`.
```php
$arrayBytes = ArrayByte::fromFormatString('01:02');
echo ArrayByte::cast('uint16_be', $arrayBytes);
```
```
258
```

## castArray
Преобразует массив байт в массив данных указанного типа.
```php
public static function castArray(string $type, array $dataBytes);
```
Используется для преобразования данных, идущих подряд в массив указанного типа.
Необходимо, что бы память была кратной указанному размеру.

Посмотреть поддерживаемые типы можно в файле `Ponikrf\Indulib\Classes\DataType`.
Если тип не найден - бросит исключение `DataTypeException`.
```php
$arrayBytes = ArrayByte::fromFormatString('01:02:01:03');
$result = ArrayByte::castArray('uint16_be', $arrayBytes);
print_r($result);
```
```
Array
(
    [0] => 258
    [1] => 259
)
```

## pack
Преобразует данные указанного типа в массив байт.
```php
public static function pack(string $type, $data)
```
Рекомендуется использовать `Ponikrf\Indulib\Classes\ArrayByteFrom`.
Посмотреть поддерживаемые типы можно в файле `Ponikrf\Indulib\Classes\DataType`.
Если тип не найден - бросит исключение `DataTypeException`.
```php
$arrayBytes = ArrayByte::pack('uint16_be', 258);
ArrayByte::printBytes($arrayBytes);
```
```
01:02
```

## packArray
Преобразует массив данных в массив байт указанного типа.
```php
public static function packArray(string $type, array $dataArray)
```
Посмотреть поддерживаемые типы можно в файле `Ponikrf\Indulib\Classes\DataType`.
Если тип не найден - бросит исключение `DataTypeException`.
```php
$arrayBytes = ArrayByte::packArray('uint16_be', [258, 259]);
ArrayByte::printBytes($arrayBytes);
```
```
01:02:01:03
```

## dump
Печатает таблицу DEC HEX ASCII BIN для каждого байта
```php
public static function dump(array $dataBytes, $return = false)
```
Если указать `$return = true` результат будет возвращен.
```php
$arrayBytes = ArrayByte::fromFormatString('01:02:03:04');
ArrayByte::dump($arrayBytes);
```
```
   DEC    HEX  ASCII        BIN
     1      1    SOH          1
     2      2    STX         10
     3      3    ETX         11
     4      4    EOT        100
```

## getBytes
Возвращает массив байт в количестве $countBytes и пропустив от начала $offsetBytes
```php
public static function getBytes(array $dataBytes, int $countBytes = 1, int $offsetBytes = 0): array
```
```php
$arrayBytes = ArrayByte::fromFormatString('01:02:03:04');
$result = ArrayByte::getBytes($arrayBytes, 2, 2);
ArrayByte::printBytes($result);
```
```
03:04
```

## getByteArray
Делит массив байт на части размером $countBytes в количестве $howMany и возвращает результат в виде двумерного массива.
```php
public static function getByteArray(array $dataBytes, int $countBytes = 1, int $howMany = 0): array
```
```php
$arrayBytes = ArrayByte::fromFormatString('01:02:03:04');
$result = ArrayByte::getByteArray($arrayBytes, 2, 2);
print_r($result);
```
```
Array
(
    [0] => Array
        (
            [0] => 1
            [1] => 2
        )

    [1] => Array
        (
            [0] => 3
            [1] => 4
        )

)
```


## randomBytes
Генерирует массив случайных байт в количестве $Bytes.
```php
public static function randomBytes(int $Bytes): array
```
```php
$arrayBytes = ArrayByte::randomBytes(4);
ArrayByte::printBytes($arrayBytes);
```
Получаем всегда разные значения
```
59:29:CB:3D
```

## remake
Пересоздает массив по порядку и перестраивает индексы от 0
```php
public static function remake(array $dataBytes): array
```

## catBytes
Преобразует 2 массива байт в один с порядком $ending
```php
public static function catBytes(array $a, array $b, bool $ending = true): array
```
```php
$a = [1,2];
$b = [3,4];
$arrayBytes = ArrayByte::catBytes($a,$b);
ArrayByte::printBytes($arrayBytes);
```
```
01:02:03:04
```

## size
Возвращает количество байт
```php
public static function size(array $arrayBytes): int
```
```php
$arrayBytes = ArrayByte::fromFormatString('01:02:03:04');
echo ArrayByte::size($arrayBytes);
```
```
4
```

## reverseBytes
Возвращает обратный порядок байт, группируя блоки байт размером $blockSize
```php
public static function reverseBytes(array $dataBytes, $blockSize = 1): array
```
```php
$arrayBytes = ArrayByte::fromFormatString('01:02:03:04:05:06:07:08');
$result = ArrayByte::reverseBytes($arrayBytes, 1);
ArrayByte::printBytes($result);
```
```
08:07:06:05:04:03:02:01
```
Группировка по 2 байта
```php
$arrayBytes = ArrayByte::fromFormatString('01:02:03:04:05:06:07:08');
$result = ArrayByte::reverseBytes($arrayBytes, 2);
ArrayByte::printBytes($result);
```
```
07:08:05:06:03:04:01:02
```
Группировка по 4 байта
```php
$arrayBytes = ArrayByte::fromFormatString('01:02:03:04:05:06:07:08');
$result = ArrayByte::reverseBytes($arrayBytes, 4);
ArrayByte::printBytes($result);
```
```
05:06:07:08:01:02:03:04
```


## fromFormatString
Преобразует форматированную строку в массив байт используя разделитель $delimiter
```php
public static function fromFormatString($strBytes, $delimiter = ':')
```
```php
$arrayBytes = ArrayByte::fromFormatString('01 02 03 04',' ');
ArrayByte::printBytes($arrayBytes);
```
```
01:02:03:04
```


## printBytes
Печатает строку байт (или возвращает) ограниченную размером $size байт
```php
public static function printBytes(array $byteArray, $return = false, $size = 0)
```
Можно вернуть результат используюя `$return = true`. Так же можно ограничить количество печатаемых байт указав размер $size.
```php
$arrayBytes = ArrayByte::fromFormatString('01:02:03:04:05:06:07:08:09');
ArrayByte::printBytes($arrayBytes,false,4);
```
```
01:02:03:04...(5)
```


## printByte
Печатает (или возвращает) HEX символ байта
```php
public static function printByte(int $byte, $return = false)
```
Можно вернуть результат используюя `$return = true`.


## isByte
Возвращает true если считает, что с точки зрения PHP данное число может преобразоватся в байт
```php
public static function isByte(int $byte)
```


## toString
Конвертирует массив байт в строку
```php
public static function toString(array $bytesArray = []): string
```
Возвращает `''` в случае если массив не может быть преобразован.
```php
$arrayBytes = ArrayByte::fromFormatString('01:02:03:04');
$stringByte = ArrayByte::toString($arrayBytes);
StringByte::printBytes($stringByte);
```
```
01:02:03:04
```
