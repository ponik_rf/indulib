Bin
=======

Класс для работы с битами.

Методы:
 * [print](#print) - Печатает (или возвращает) битовое представление числа
 * [getBit](#getbit) - Возвращает значение бита (первым битом считается нулевой бит)
 * [bits](#bits) - Возвращает маску бит заполненную единицами в количестве $bits
 * [setBit](#setbit) - Устанавливает указанный бит в единицу (первым битом считается нулевой бит)
 * [clearBit](#clearbit) - Устанавливает указанный бит в ноль (первым битом считается нулевой бит)
 * [low](#low) - Возвращает младший байт
 * [high](#high) - Возвращает старший байт
 * [unitCount](#unitcount) - Считает количество битовых единиц


## print
Печатает (или возвращает) битовое представление числа
```php
public static function print(int $number, bool $return = false)
```
```php
Bin::print(128)
```
```
0b10000000
```


## getBit
Возвращает значение бита (первым битом считается нулевой бит)
```php
public static function getBit($byte, int $bit = 0): int
```
```php
// 128 = 0b10000000
// 8 бит единица но указывается 7 потому что считается от ноля
echo Bin::getBit(128, 7);
```
```
1
```


## bits
Возвращает маску бит заполненную единицами в количестве $bits
```php
public static function bits(int $bits): int
```
```php
$bin = Bin::bits(4);
Bin::print($bin);
```
```
0b1111
```


## setBit
Устанавливает указанный бит в единицу (первым битом считается нулевой бит)
```php
public static function setBit($byte, int $bit = 0): int
```
```php
$bin = Bin::setBit(128,0);
Bin::print($bin);
```
```
0b10000001
```


## clearBit
Устанавливает указанный бит в ноль (первым битом считается нулевой бит)
```php
public static function clearBit($byte, int $bit = 0): int
```
```php
$bin = Bin::clearBit(255,4);
Bin::print($bin);
```
```
0b11101111
```


## low
Возвращает младший байт
```php
public static function low($twoByte): int
```
```php
$bin = Bin::low(26265); // 26265 = 0b0110 0110 1001 1001
Bin::print($bin);
```
```
0b10011001
```


## high
Возвращает старший байт
```php
public static function high($twoByte): int
```
```php
$bin = Bin::high(26265); // 26265 = 0b0110 0110 1001 1001
Bin::print($bin);
```
```
0b1100110
```


## unitCount
Считает количество битовых единиц
```php
public static function unitCount(int $number): int
```
```php
echo Bin::unitCount(26265); // 26265 = 0b0110 0110 1001 1001
```
```
8
```