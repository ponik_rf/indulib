BCD
=====

Класс для работы с двоично десятичным кодом. (BCD формат)

 * [Статические размеры](#статические-размеры)
 * [Примеры использования](#примеры-использования)

Методы:
 * [cast](#cast) - Преобразует BCD строку или массив байт в число.
 * [pack](#pack) - Возвращает **строку** байт в формате BCD, преобразованную из числа $number размером $size байт.

## Статические размеры

Традиционно BCD формат не имеет конкретной длинны, но в `DataType` можно найти массив `BCDType` который содержит поддерживаемые статические размеры. То есть, функции `StringByte::cast` или `ArrayByte:cast` поддерживают преобразование BCD типов со статическим указанием размера данных.


Поддерживаемые статические размеры:
 * **BCD8** размер 1 байт
 * **BCD16** размер 2 байт
 * **BCD24** размер 3 байт
 * **BCD32** размер 4 байт
 * **BCD48** размер 6 байт
 * **BCD64** размер 8 байт

### Примеры использования

Упаковка числа в BCD:
```php
$stringBytes = StringByte::pack('BCD24',123456);
StringByte::printBytes($stringBytes);
```
```
12:34:56
```

Выравнивание числа происходит по правой стороне:
```php
$stringBytes = StringByte::pack('BCD24',1234);
StringByte::printBytes($stringBytes);
```
```
00:12:34
```

Распаковка BCD данных:
```php
$stringBytes = StringByte::fromFormatString('12:34:56');
echo StringByte::cast('BCD24',$stringBytes);
```
```
123456
```


## cast
Преобразует BCD строку или массив байт в число. Можно ограничить количество используемых байт параметром $size.

```php
public static function cast($dataBytes, int $size = 12)
```
```php
$stringBytes = StringByte::fromFormatString('12:34:56:78:90');
echo BCD::cast($stringBytes);
```
```
1234567890
```


## pack
Возвращает **строку** байт в формате BCD преобразованную из числа $number размером $size байт. Выравнивание числа происходит по правой стороне.

```php
public static function pack(int $number, int $size)
```
```php
$stringBytes = BCD::pack(1234, 3);
StringByte::printBytes($stringBytes);
```
```
00:12:34
```
