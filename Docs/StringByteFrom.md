StringByteFrom
=============

Класс для преобразования данных в строку байт. Класс является магической оберткой вокруг `StringByte::pack`. Преобразует все стандартные типы указанные из `DataType.php` в строку байт.

## Использование

```php
$stringBytes = StringByteFrom::uint32_be(4294967295);
StringByte::printBytes($stringBytes);
```
```
FF:FF:FF:FF
```

## Список функций
```php
// Basic pack types:
public static function char($data);
public static function int8($data);
public static function uint8($data);
public static function int16($data);
public static function uint16($data);
public static function uint16_be($data);
public static function uint16_le($data);
public static function int32($data);
public static function uint32($data);
public static function uint32_be($data);
public static function uint32_le($data);
public static function int64($data);
public static function uint64($data);
public static function uint64_be($data);
public static function uint64_le($data);
public static function float($data);
public static function float_be($data);
public static function float_le($data);
public static function double($data);
public static function double_be($data);
public static function double_le($data);

// BCD types:
public static function BCD8($data);
public static function BCD16($data);
public static function BCD24($data);
public static function BCD32($data);
public static function BCD48($data);
public static function BCD64($data);

// CRC type:
public static function CRC8($data);
public static function CRC16($data);
public static function CRC32($data);
```