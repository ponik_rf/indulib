ASCII
=======

Класс для работы с ASCII символами

Полезные константы:

 * [ASCII_INVISIBLE_CODES](#ascii_invisible_codes) - Массив невидимых символов ASCII.
 * [ASCII_CODES](#ascii_codes) - Все коды ASCII.

Методы:
 * [format](#format) - Приводит строку или массив байт к ASCII формату.
 * [addControlBit](#addcontrolbit) - Добавляет к строке или массиву байт бит четности.
 * [printBytes](#printbytes) - Печатает ASCII строку заменяя несуществущие символы их определением.


## ASCII_INVISIBLE_CODES

Список ASCII непечатаемых символов

Определение:
```php
    const ASCII_INVISIBLE_CODES = [
    'NUL', 'SOH', 'STX', 'ETX', 'EOT', 'ENQ',
    'ACK', 'BEL', 'BS', 'HT', 'LF', 'VT', 'FF',
    'CR', 'SO', 'SI', 'DLE', 'DC1', 'DC2',
    'DC3', 'DC4', 'NAK', 'SYN', 'ETB', 'CAN',
    'EM','SUB', 'ESC', 'FS', 'GS', 'RS', 'US',
    'SPC',
    0x7F => 'DEL',
];
```

Использование:

```php
$char = 15;
if (array_key_exists($char, ASCII::ASCII_INVISIBLE_CODES)) {
    echo "invisible \n";
}
```
```php
invisible
```


## ASCII_CODES

Полный список ASCII кодов

Определение:
```php
const ASCII_CODES = [
    'NUL', 'SOH', 'STX', 'ETX', 'EOT', 'ENQ', 'ACK', 'BEL', 'BS', 'HT', 'LF', 'VT', 'FF', 'CR', 'SO', 'SI',
    'DLE', 'DC1', 'DC2', 'DC3', 'DC4', 'NAK', 'SYN', 'ETB', 'CAN', 'EM', 'SUB', 'ESC', 'FS', 'GS', 'RS', 'US',
    'SPC', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/',
    '0', '1', '2', '3', '4', '5', '6', "7", '8', '9', ':', ';', '<', '=', '>', '?',
    '@', 'A', 'B', 'C', 'D', 'E', 'F', "G", 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T', 'U', 'V', "W", 'X', 'Y', 'Z', '[', '\\', ']', '^', '_',
    '`', 'a', 'b', 'c', 'd', 'e', 'f', "g", 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', "w", 'x', 'y', 'z', '{', '|', '}', '~', 'DEL',
];
```

Использование:

```php
$chars = [0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6C, 0x64, 0x0D];
foreach ($chars as $char) {
    echo ASCII::ASCII_CODES[$char];
}
```
```
HelloSPCworldCR
```


## format

Приводит строку или массив байт к ASCII формату. Позволяет быстро убрать бит четности для корректного отображения текста. Производит модифицирование непосредственно переданого значения. Возвращает `true` в случае успеха.

```php
public static function format(&$mixed)
```
```php
$test = 'Hello world';
StringByte::printBytes($test);
ASCII::addControlBit($test);
StringByte::printBytes($test);
ASCII::format($test);
StringByte::printBytes($test);
```
```
48:65:6C:6C:6F:20:77:6F:72:6C:64
48:65:6C:6C:6F:A0:77:6F:72:6C:E4
48:65:6C:6C:6F:20:77:6F:72:6C:64
```


## addControlBit

Добавляет к строке или массиву байт бит четности. Производит модифицирование непосредственно переданого значения. Возвращает `true` в случае успеха.

```php
public static function addControlBit(&$mixed)
```
```php
$test = 'Hello world';
StringByte::printBytes($test);
ASCII::addControlBit($test);
StringByte::printBytes($test);
```
```
48:65:6C:6C:6F:20:77:6F:72:6C:64
48:65:6C:6C:6F:A0:77:6F:72:6C:E4
```

## printBytes

Печатает ASCII строку заменяя несуществущие символы их определением.

Настройки печати:
```php
public static $DEFAULT_PRINT_OPTIONS = [
    'start_nc' => '',  // Начало нормального символа
    'end_nc' => '',    // Окончание нормального символа
    'start_cc' => '',  // Начало управляющего символа
    'end_cc' => '',    // Конец управляющего символа
    'use_CR' => true,   // Печатает символ перевода строки вместе с абривиатурой
    'return' => false,  // возврат результата вместо печати
];
```

```php
public static function printBytes($mixed, $options = [])
```
```php
$test = 'Hello world';
ASCII::printBytes($test, [
    'start_cc' => '[',
    'end_cc' => ']',
]);
```
```
Hello[SPC]world
```

Иногда полезно не печатать перевод строки, оставив лишь определение:
```php
$test = "Hello\nworld";
ASCII::printBytes($test, [
    'start_cc' => '[',
    'end_cc' => ']',
    'use_CR' => false
]);
```
```
Hello[LF]world
```

