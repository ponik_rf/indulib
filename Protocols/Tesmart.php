<?php

namespace Ponikrf\Indulib\Protocols;

use Ponikrf\Indulib\Classes\ArrayByte;
use Ponikrf\Indulib\Classes\ArrayByteFrom;
use Ponikrf\Indulib\Classes\ArrayByteTo;
use Ponikrf\Indulib\Classes\Bin;
use Ponikrf\Indulib\Classes\BufferByte;
use Ponikrf\Indulib\Classes\CRC;
use Ponikrf\Indulib\Classes\StringByte;
use Ponikrf\Indulib\Classes\StringByteFrom;
use Ponikrf\Indulib\Classes\StringByteTo;
use Psy\Util\Str;

/**
 * Basic Tesma106 protocol
 *
 *  @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Tesmart
{
    /**
     * Create a new package for send
     *
     * @param int $address
     * @param int $cmd
     * @param int $subCommand
     * @param string $dataByte
     * @return string
     */
    public static function newPackage(int $address, int $cmd, int $subCommand, string $dataByte): string
    {
        $bb = new BufferByte();

        $addrByte = ArrayByteTo::string([$address]);

        $addrBytes = StringByte::catBytes($addrByte, ~$addrByte);

        $newCrc = function ($bb) {
            return Tesmart::CRC8($bb->toString());
        };

        return $bb->init()
            ->addArray([0x55])
            ->addString($addrBytes)
            ->addArray([$cmd, $subCommand, strlen($dataByte)])
            ->addString($dataByte)
            ->addFunction($newCrc)
            ->toString();
    }

    /**
     * Split byte array
     *
     * @param string $byteData
     * @return array
     */
    public static function splitPackage(string $byteData): array
    {
        $bb = new BufferByte();

        return $bb->init($byteData)
            ->addBlock('sig', 1, 0)
            ->addBlock('address', 1, 1)
            ->addBlock('!address', 1, 2)
            ->addBlock('command', 1, 3)
            ->addBlock('subCommand', 1, 4)
            ->addBlock('size', 1, 5)
            ->addBlock('data', strlen($byteData) - 7, 6)
            ->addBlock('CRC', 1, strlen($byteData) - 1)
            ->blocks();
    }

    /**
     * Check package CRC
     *
     *
     */
    public static function checkCRC(string $byteData): bool
    {
        $my_CRC = self::CRC8(StringByte::getBytes($byteData, strlen($byteData) - 1, 0));
        $CRC = StringByte::getBytes($byteData, 1, strlen($byteData) - 1);
        if ($my_CRC == $CRC) {
            return true;
        }
        return false;
    }

    /**
     * Partial package check
     *
     * @param string $byteData
     * @return bool
     */
    public static function checkPackage(string $byteData): bool
    {
        if (strlen($byteData) > 6) {
            if ((StringByteTo::uint8($byteData{5}) + 7) == strlen($byteData)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Convert address byte to address
     *
     * @param string $byteData
     * @return int
     */
    public static function convertAddress(string $byteData): int{
        return StringByteTo::uint8($byteData);
    }

    /**
     * Generate tesma CRC8
     *
     * @param string $dataBytes
     * @return mixed
     */
    public static function CRC8(string $dataBytes): string
    {
        $bArray = StringByteTo::array($dataBytes);
        $sum = 0;
        foreach ($bArray AS $b) {
            $sum += $b;
        }
        return StringByteFrom::uint8(Bin::low(~$sum));
    }
}