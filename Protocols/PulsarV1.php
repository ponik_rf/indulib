<?php

namespace Ponikrf\Indulib\Protocols;

use Ponikrf\Indulib\Classes\ArrayByte;
use Ponikrf\Indulib\Classes\BufferByte;
use Ponikrf\Indulib\Classes\CRC;
use Ponikrf\Indulib\Classes\StringByte;
use Ponikrf\Indulib\Classes\StringByteFrom;
use Ponikrf\Indulib\Classes\StringByteTo;
use Psy\Util\Str;


/**
 * Basic pulsar protocol
 *
 * LOW ---> HIGH
 * ------------>           ------------> <-----> ------>
 * +-------------+----+----+-------------+-------+-------+
 * |      4      |  1 | 1  |     4       |   2   |   2   |
 * |   address   |  F | L  |  MASC_CH    |   ID  | CRC16 |
 * +-------------+----+----+-------------+-------+-------+
 * |     99295   |  1 | 14 |      1      |       |       |
 * |  0  1  2  3 |  4 |  5 |  6  7  8  9 | 10 11 | 12 13 |
 * | 00 09 92 95 | 01 | 0E | 01 00 00 00 | F6 6C | 16 2F |
 * |-----------------------------------------------------|
 *
 *  @author Boris Bobylev <ponik_rf@mail.ru>
 */
class PulsarV1
{
    /**
     * Get type package generate for send
     *
     * @param int $address
     * @return string
     * @throws \Exception
     */
    public static function newTypePackage(int $address): string
    {
        $bb = new BufferByte();

        return $bb->init()
            ->addString(StringByteFrom::BCD32($address, 4))
            ->addArray([0x03, 0x02, 0x46, 0x00, 0x01])
            ->addCRC16()
            ->toString();
    }

    /**
     * Split type package
     *
     * @param $byteData
     * @return mixed
     */
    public static function splitTypePackage($byteData): array
    {

        $bb = new BufferByte();

        $my_CRC = CRC::CRC16(StringByte::getBytes($byteData, 2, strlen($byteData) - 2));

        return $bb->init($byteData)
            ->addBlock('address', 4, 0)
            ->addBlock('command', 2, 4)
            ->addBlock('type', 2, 6)
            ->addBlock('CRC', 2, strlen($byteData) - 2)
            ->addBlockString('my_CRC', $my_CRC)
            ->blocks();
    }


    /**
     * Create a new package for send
     *
     * @param int $address
     * @param int $cmd
     * @param string $dataByte
     * @param string $ids
     * @return string
     * @throws \Exception
     */
    public static function newPackage(int $address, int $cmd, string $dataByte, string $ids = ''): string
    {
        $bb = new BufferByte();

        return $bb->init()
            ->addString(StringByteFrom::BCD32($address, 4))
            ->addArray([$cmd, 10 + strlen($dataByte)])
            ->addString($dataByte)
            ->addString($ids)
            ->addCRC16()
            ->toString();
    }

    /**
     * Check package CRC
     *
     * @param $byteData
     * @return bool
     */
    public static function checkCRC(string $byteData){
        $my_CRC = CRC::CRC16(StringByte::getBytes($byteData, strlen($byteData) - 2, 0));
        $package_CRC = StringByte::getBytes($byteData, 2, strlen($byteData) - 2);
        if ($my_CRC == $package_CRC){
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Split byte array
     *
     * @param string $byteData
     * @return array
     */
    public static function splitPackage(string $byteData): array
    {
        $bb = new BufferByte();
        return $bb->init($byteData)
            ->addBlock('address', 4, 0)
            ->addBlock('command', 1, 4)
            ->addBlock('size', 1, 5)
            ->addBlock('data', strlen($byteData) - 10, 6)
            ->addBlock('id', 2, strlen($byteData) - 4)
            ->addBlock('CRC', 2, strlen($byteData) - 2)
            ->blocks();
    }

    /**
     * Convert address byte to address
     *
     * @param string $byteData
     * @return int
     */
    public static function convertAddress(string $byteData): int{
        return StringByteTo::BCD32($byteData);
    }

    /**
     * Partial package check
     *
     * @param $byteData
     * @return bool
     */
    public static function checkPackage(string $byteData): bool
    {
        if (strlen($byteData) > 6) {
            if (StringByteTo::uint8($byteData{5}) == strlen($byteData)) {
                return true;
            }
        }
        return false;
    }
}