<?php

namespace Ponikrf\Indulib\Protocols;

/**
 *
 * Класс описывет общие константы и функции для работы с протоколом Modbus
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Modbus
{
    /**
     *
     * Бит определяющий, что данная команда - сообщение об ошибке
     *
     */
    const ERROR_BIT = 0x80;

    /*****************************************
     *
     *  НАСТРОЙКА И ОПИСАНИЕ КОМАНД
     *
     *****************************************/

    /**
     * Все стандартные команды ModBus RTU
     *
     * Команды могут быть модифицированы устройством самим устройством
     * что бы ограничить доступ к необрабатываемым командам самого устройства
     *
     */
    const commands = [

        /**
         * Чтение данных
         *
         * Запрос состоит из адреса первого элемента таблицы,
         * значение которого требуется прочитать, и количества считываемых элементов.
         * Адрес и количество данных задаются 16-битными числами,
         * старший байт каждого из них передается первым.
         *
         */
        0x01 => "Read Coil Status",               // чтение значений из нескольких регистров флагов
        0x02 => "Read Discrete Inputs",           // чтение значений из нескольких дискретных входов
        0x03 => "Read Holding Registers",         // чтение значений из нескольких регистров хранения
        0x04 => "Read Input Registers",           // чтение значений из нескольких регистров ввода

        /**
         * Запись одного значения
         *
         * Команда состоит из адреса элемента (2 байта) и устанавливаемого значения (2 байта).
         * Для регистра хранения значение является просто 16-битным словом.
         * Для флагов значение 0xFF00 означает включённое состояние, 0x0000 — выключенное,
         * другие значения недопустимы.
         *
         */
        0x05 => "Force Single Coil",              // запись значения одного флага
        0x06 => "Preset Single Register",         // запись значения в один регистр хранения

        /**
         * Запись нескольких значений
         *
         * Команда состоит из адреса элемента, количества изменяемых элементов,
         * количества передаваемых байт устанавливаемых значений и самих устанавливаемых значений.
         * Данные упаковываются так же, как в командах чтения данных.
         *
         */
        0x0F => "Force Multiple Coils",           // запись значений в несколько регистров флагов
        0x10 => "Preset Multiple Registers",      // запись значений в несколько регистров хранения

        /**
         * Изменение регистров
         *
         * Команда состоит из адреса регистра и двух 16-битных чисел,
         * которые используются как маски, с помощью которых можно индивидуально сбросить
         * или установить отдельные биты в регистре.
         * Конечный результат определяется формулой:
         *
         * Результат = (Текущее_значение AND Маска_И) OR (Маска_ИЛИ AND (NOT Маска_И))
         *
         */
        0x16 => "Mask Write Register",            // запись в один регистр хранения с использованием маски "И" и маски "ИЛИ"

        /**
         * Очереди данных
         *
         * Функция предназначена для получения 16-битных слов из очереди,
         * организованной по принципу «первым пришёл — первым ушёл» (FIFO).
         *
         */
        0x18 => "Read FIFO Queue",                // чтение данных из очереди

        /**
         * Доступ к файлам
         *
         * Эти функции используются для доступа к 16-битным регистрам,
         * организованным в файлы, состоящие из записей произвольной длины.
         * В команде указывается номер файла, номер записи и длина записи в 16-битных словах.
         * С помощью одной команды можно записать или прочитать несколько записей, не обязательно соседних.
         * Кроме того, команда содержит однобайтовый код для указания типа ссылки на данные.
         * В действующей версии стандарта определен только один тип (описанный выше) с кодом 0x06.
         *
         */
        0x14 => "Read File Record",               // чтение из файла
        0x15 => "Write File Record",              // запись в файл

        /**
         * Диагностика
         */
        0x07 => "Read Exception Status",          // Чтение сигналов состояния
        // Функция предназначена для получения информации об индикаторах состояния
        // на удалённом устройстве. Функция возвращает один байт,
        // каждый бит которого соответствует состоянию одного индикатора.

        0x08 => "Diagnostic",                     // Диагностика
        0x0B => "Get Com Event Counter",          // Чтение счетчика событий
        0x0C => "Get Com Event Log",              // Чтение журнала событий
        // Эти функции предназначены для проверки функционирования последовательной линий связи.

        0x11 => "Report Slave ID",                // Чтение информации об устройстве

        /**
         * Другие
         *
         * Функция предназначена для передачи данных в произвольных форматах
         * (определённых другими стандартами) от ведущего (master) к ведомому (slave) и обратно.
         * Тип передаваемых данных определяется дополнительным кодом
         * (MEI — MODBUS Encapsulated Interface), передаваемым после номера функции.
         * Стандарт определяет
         * MEI 13 (0x0D), предназначенный для инкапсуляции протокола CANopen.
         * MEI 14 (0x0E) используется для получения информации об устройстве
         * MEI в диапазонах 0—12 и 15—255 зарезервированы.
         *
         */
        0x2B => "Encapsulated Interface Transport",
    ];

    /**
     * Список логических ошибок
     * логические ошибки - запрос принят без искажений, но не может быть выполнен
     *
     * Признаком того, что ответ содержит сообщение об ошибке,
     * является установленный старший бит номера функции.
     * За номером функции, вместо обычных данных, следует код ошибки и,
     * при необходимости, дополнительные данные об ошибке.
     *
     */
    const errorCodes = [
        0x01 => "Принятый код функции не может быть обработан",
        0x02 => "Адрес данных, указанный в запросе, недоступен",
        0x03 => "Значение, содержащееся в поле данных запроса, является недопустимой величиной",
        0x04 => "Невосстанавливаемая ошибка имела место, пока ведомое устройство пыталось выполнить затребованное действие",
        0x05 => "Ведомое устройство приняло запрос и обрабатывает его, но это требует много времени",
        0x06 => "Ведомое устройство занято обработкой команды",
        0x07 => "Ведомое устройство не может выполнить программную функцию, заданную в запросе",
        0x08 => "Ведомое устройство при чтении расширенной памяти обнаружило ошибку контроля четности",
    ];

    /**
     *
     * Проверяет, является ли это команда ответом ошибки
     *
     * @param $command
     * @return bool
     */
    public static function isErrorCommand($command)
    {
        if (($command & self::ERROR_BIT) != 0) {
            return true;
        }
        return false;
    }


}