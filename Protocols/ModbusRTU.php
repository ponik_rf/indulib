<?php

namespace Ponikrf\Indulib\Protocols;

use Ponikrf\Indulib\Classes\ArrayByte;
use Ponikrf\Indulib\Classes\Memory;
use Ponikrf\Indulib\Classes\MemoryCast;
use Ponikrf\Indulib\Classes\MemoryDump;
use Ponikrf\Indulib\Classes\MemoryRule;
use Ponikrf\Indulib\Classes\Package;
use Ponikrf\Indulib\Classes\StringByteTo;
use Ponikrf\Indulib\Exceptions\ProtocolException;
use \Ponikrf\Indulib\Providers\ProviderInterface;

/**
 *
 * Класс для работы с протоколом Modbus RTU
 *
 * @author Boris Bobylev <ponik_rf@mail.ru>
 */
class ModbusRTU
{
    /*****************************************
     *
     * ОБЪЕКТЫ
     *
     *****************************************/

    /**
     * Класс реализующий ProviderInterface
     */
    protected $Provider;

    /*****************************************
     *
     * НАСТРОЙКИ СОЕДИНЕНИЯ И ТАЙМАУТЫ ЗАПРОСОВ
     *
     *****************************************/

    /**
     * Таймаут на счтения с провайдера
     */
    protected $readTimeout = [
        'second' => 1,
        'microsecond' => 0,
    ];

    /**
     * Таймаут на чтения чистки буфера
     */
    protected $preRequestTimeout = [
        'second' => 0,
        'microsecond' => 200000,
    ];

    /**
     * Сколько раз пытатся прочитать
     */
    protected $readErrors = 3;

    /*****************************************
     *
     *  НАСТРОЙКА И ОПИСАНИЕ КОМАНД
     *
     *****************************************/

    /**
     * Позволяет переопределять собственные сценарии быстрой проверки пакета
     *
     * В качестве ключей массива назначается команда а в качестве значений функция
     *
     * Пример для переопределения стандартной команды 0x04:
     *
     * ```PHP
     *
     * $this->checkPackage[0x04] = function ($buffer){
     *      if (StringByteTo::int8($byteData{2}) == (strlen($byteData) - 5)) {
     *          return true;
     *      }
     *      return false;
     * }
     *
     * ```
     *
     */
    protected $checkPackage = [];

    /**
     * Позволяет переопределять правила каста для ответа
     *
     * Работает аналогично атрибуту $this->checkPackage
     * только в качестве значения необходимл указывать
     * правило MemoryRule
     */
    protected $castResponse = [];

    protected $debug = false;

    /**
     * Доступные команды
     *
     * Если передоваемая команда не входит в этот список, пакет не будет считатся валидным
     * и будет обрабатыватся, как ошибочный.
     *
     * Это относится и к передоваемым командам и к получаемым
     *
     */
    protected $availableCommands = [
        0x01, 0x02, 0x03, 0x04,
        0x05, 0x06, 0x0F, 0x10,
        0x16, 0x18, 0x14, 0x15,
        0x07, 0x08, 0x0B, 0x0C,
        0x11, 0x2B,
    ];


    public function __construct(ProviderInterface $Provider)
    {
        $this->Provider = $Provider;
    }

    public function debug($debug){
        $this->debug = $debug;
    }

    /**
     * Проверяет доступность команды
     *
     * @param $command
     * @return bool
     */
    public function availableCommand($command)
    {
        return in_array($command, $this->availableCommands);
    }

    /**
     *
     * Создает мастер пакет для отправки в сеть
     *
     * @param int $address Адрес назначения
     * @param int $cmd Команда
     * @param int $start_address Начальный адрес
     * @param int $count Количество
     * @param string $data Данные для передачи
     * @return Package
     * @throws \Ponikrf\Indulib\Exceptions\DataTypeException
     */
    public function createMasterPackage(int $address, int $cmd, int $start_address, int $count, string $data = '')
    {
        if ($data) {
            $Package = (new Package())
                ->uint8($address)
                ->uint8($cmd)
                ->uint16_be($start_address)
                ->uint16_be($count)
                ->uint8(strlen($data))
                ->stringData($data)
                ->CRC16();
        } else {
            $Package = (new Package())
                ->uint8($address)
                ->uint8($cmd)
                ->uint16_be($start_address)
                ->uint16_be($count)
                ->CRC16();
        }
        return $Package;
    }

    /**
     *
     * Запрос через пройвайдер
     *
     * @param int $address (uint8)
     * @param int $cmd Команда      (uint8)
     * @param int $start_address (uint16_be)
     * @param int $count (uint16_be)
     * @param string $data String Bytes
     * @return array
     * @throws
     */
    public function request(int $address, int $cmd, int $start_address, int $count, string $data = '')
    {
        if (!$this->availableCommand($cmd)) {
            $Byte = ArrayByte::printByte($cmd);
            throw new ProtocolException("Неизвестная команда отправки ($Byte)", 81002);
        }

        $Package = $this->createMasterPackage($address, $cmd, $start_address, $count, $data);

        if ($this->debug) $Package->dump();

        try {
            $this->Provider->request(
                $Package->toString(),
                $this->readTimeout['second'],
                $this->readTimeout['microsecond'],
                3,
                function ($dataByte) {
                    return $this->checkPackage($dataByte);
                }
            );
        } catch (ProtocolException $e) {
            $ResponseDump = (new MemoryDump())
                ->fromString($this->Provider->getReadBuffer())
                ->setCaption("RESPONSE")
                ->dump(true);

            $e->setData($Package->dump(true) . $ResponseDump);
            throw $e;
        }

        /* Если ничего не получено то устройство не отвечает */
        if (!$this->Provider->getReadBuffer()) {
            throw (new ProtocolException("Ответ не был получен (Timeout)", 81001))
                ->setData($Package->dump(true));
        }

        $Cast = $this->castResponse($address,$cmd);

        if ($this->debug) $Cast->dump();

        // Если что то не так с пакетом
        if ($Cast->getStatus() != MemoryCast::STATUS_SUCCESS) {
            throw (new ProtocolException("Неверный формат полученых данных", 81003))
                ->setData($Package->dump(true) . $Cast->dump(true));
        }

        $result = $Cast->result();

        // Проверяем является ли результат ошибкой
        if (Modbus::isErrorCommand($result['command'])) {
            throw (new ProtocolException(Modbus::errorCodes[$result['error']], 81000))
                ->setData($Package->dump(true) . $Cast->dump(true));
        }

        return $result;
    }

    /**
     * Всеми силами пытается кастануть полученные данные
     *
     * @todo допилить поддержку всех команд
     *
     * В зависимости от команды мы опять же применяем разные правила
     * @param $address
     * @param $command
     * @return MemoryCast
     * @throws ProtocolException
     */
    protected function castResponse($address,$command)
    {
        $byteData = $this->Provider->getReadBuffer();

        $cmd = StringByteTo::uint8($byteData{1});

        $Mem = (new Memory())->set($byteData);

        $Rule = (new MemoryRule())
            ->uint8('address',['values'=>[$address]])
            ->uint8('command',['values'=>[$command,$command|Modbus::ERROR_BIT]]);

        /**
         * Выбор правила по которму будет разделятся наш ответ
         */
        switch ($cmd) {
            case 0x01:
            case 0x02:
            case 0x03:
            case 0x04:
                $Rule->uint8("size", ['bind' => 'size'])
                    ->data("data", function ($binds) {
                        return $binds['size'] - 10;
                    })
                    ->CRC16();
                break;
            case 0x05;
            case 0x06;
                $Rule->uint16_be("register")
                    ->uint16_be("value")
                    ->CRC16();
                break;
            case 0x0F;
            case 0x10:
            case 0x11:
                $Rule->uint16_be("register")
                    ->uint16_be("count")
                    ->CRC16();
                break;
            default:

                /**
                 * Обработка ответов с ошибкой
                 */
                if (Modbus::isErrorCommand($cmd)) {
                    $Rule->uint8("error")
                        ->CRC16();
                    break;
                }

                /**
                 * Обработка собствынных правил
                 */
                if (array_key_exists($cmd, $this->castResponse)) {
                    if ($this->castResponse[$cmd] instanceof MemoryRule) {
                        $Rule = $this->castResponse[$cmd];
                        break;
                    }
                }
                throw new ProtocolException("Не возможно проверить целостность ответа, нет правила памяти для этой команды", 81003);
        }

        return new MemoryCast($Mem, $Rule, true);
    }

    /**
     * Быстрая проверка полноценности длинны пакета
     *
     *
     * @todo допилить поддержку всех команд
     *
     * @param $byteData
     * @return bool
     * @throws ProtocolException
     */
    protected function checkPackage($byteData): bool
    {
        if (strlen($byteData) < 5) return false;

        $cmd = StringByteTo::uint8($byteData{1});

        /**
         * Если команда является ошибкой
         */
        if (Modbus::isErrorCommand($cmd)) {
            if (strlen($byteData) == 5) {
                return true;
            }
            return false;
        }

        /**
         * Переопределение стандартного повидения
         */
        if (array_key_exists($cmd, $this->checkPackage))
            return ($this->checkPackage[$cmd])($byteData);


        /**
         * Обработка стандартных команд
         */
        switch (StringByteTo::uint8($byteData{1})) {
            case 0x01:
            case 0x02:
            case 0x03:
            case 0x04:
                if (StringByteTo::int8($byteData{2}) == (strlen($byteData) - 5)) {
                    return true;
                }
                break;
            case 0x05;
            case 0x06;
            case 0x0F;
            case 0x10:
            case 0x11:
                if (strlen($byteData) == 8) {
                    return true;
                }
                break;
            default:
                throw new ProtocolException("Не возможно проверить целостность пакета, нет сценария проверки этой команды", 81002);
        }
        return false;
    }


}