<?php

namespace Ponikrf\Indulib\Protocols;

use Ponikrf\Indulib\Classes\BufferByte;
use Ponikrf\Indulib\Classes\CRC;
use Ponikrf\Indulib\Classes\StringByte;
use Ponikrf\Indulib\Classes\StringByteFrom;
use Ponikrf\Indulib\Classes\StringByteTo;

/**
 * Mercury206 protocol
 *
 * Request
 *
 * +-------------+----+-------+
 * |      4      |  1 |   2   |
 * |   address   |  F | CRC16 |
 * +-------------+----+-------+
 * |     99295   |  1 |       |
 * |  0  1  2  3 |  4 |  5  6 |
 * | 00 09 92 95 | 01 | 16 2F |
 * |-------------+----+-------|
 *
 *
 * Response
 * ------------>           ------------> <-----> ------>
 * +-------------+----+----+-------------+-------+-------+
 * |      4      |  1 | 1  |     4       |   2   |   2   |
 * |   address   |  F | L  |  MASC_CH    |   ID  | CRC16 |
 * +-------------+----+----+-------------+-------+-------+
 * |     99295   |  1 | 14 |      1      |       |       |
 * |  0  1  2  3 |  4 |  5 |  6  7  8  9 | 10 11 | 12 13 |
 * | 00 09 92 95 | 01 | 0E | 01 00 00 00 | F6 6C | 16 2F |
 * |-----------------------------------------------------|
 *  @author Boris Bobylev <ponik_rf@mail.ru>
 */
class Mercury
{
    /**
     * Create a new package for send
     *
     * @param int $address
     * @param int $cmd
     * @return string
     */
    public static function newPackage(int $address, int $cmd): string
    {
        $bb = new BufferByte();

        return $bb->init()
            ->addString(StringByteFrom::uint32_be($address))
            ->addArray([$cmd])
            ->addCRC16()
            ->toString();
    }


    /**
     * Check package CRC
     *
     * @param $byteData
     * @return bool
     */
    public static function checkCRC(string $byteData){
        $my_CRC = CRC::CRC16(StringByte::getBytes($byteData, strlen($byteData) - 2, 0));
        $package_CRC = StringByte::getBytes($byteData, 2, strlen($byteData) - 2);
        if ($my_CRC == $package_CRC){
            return TRUE;
        }
        return FALSE;
    }


    /**
     * Split byte array
     *
     * @param string $byteData
     * @return array
     */
    public static function splitPackage(string $byteData): array
    {
        $bb = new BufferByte();

        return $bb->init($byteData)
            ->addBlock('address', 4, 0)
            ->addBlock('command', 1, 4)
            ->addBlock('data', strlen($byteData) - 7, 5)
            ->addBlock('CRC', 2, strlen($byteData) - 2)
            ->blocks();
    }

    /**
     * Check package
     *
     * @param $byteData
     * @return bool
     */
    public static function checkPackage(string $byteData): bool
    {
        if (strlen($byteData) > 7) {
            return true;
        }
        return false;
    }

    /**
     * Convert address byte to address
     *
     * @param string $byteData
     * @return int
     */
    public static function convertAddress(string $byteData): int{
        return StringByteTo::uint32_be($byteData);
    }
}