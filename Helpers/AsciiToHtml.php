<?php

namespace Ponikrf\Indulib\Helpers;

/**
 * Класс преобразования консольного вывода в HTML
 *
 * @author ponikrf <ponik_rf@mail.ru>
 */
class AsciiToHtml
{
    /**
     * Цвета по внутренним кодам ASCII консольного цвета
     *
     *
     */
    public static $colors = [
        'black',
        'maroon',
        'rgb(23, 90, 23)',
        'rgb(231, 231, 37)',
        'navy',
        'purple',
        'rgb(87, 194, 197)',
        'silver',
        'gray',
        'red',
        'lime',
        'yellow',
        'blue',
        'fuchsia',
        'aqua',
        '#eee'
    ];

    /**
     * Возвращает HTML преобразованную строку в которой
     * заменены все сущности консольной подсветки в HTML
     *
     * @param $str
     * @return mixed|null|string|string[]
     */
    public static function render($str)
    {
        $str = self::asciiToEntities($str);
        // Replace ANSI codes.
        $str = preg_replace_callback('/(?:\e\[\d+(?:;\d+)*m)+/', 'self::ansiDecode', "$str\033[0m");
        // Strip ASCII bell.
        $str = str_replace("\007", '', $str);
        return $str;
    }

    protected static function asciiToEntities($string)
    {
        for ($i = 128; $i <= 255; $i++) {
            $entity = htmlentities(chr($i), ENT_QUOTES);
            $temp = substr($entity, 0, 1);
            $temp .= substr($entity, -1, 1);
            if ($temp != '&;') $string = str_replace(chr($i), '', $string);
            else $string = str_replace(chr($i), $entity, $string);

        }
        return $string;
    }

    protected static function ansiDecode($matches)
    {
        static $styles = [
            'background' => null,
            'blink' => false,
            'bold' => false,
            'color' => null,
            //'inverse'      => false,
            'italic' => false,
            'line-through' => false,
            'underline' => false,
        ];

        static $css = '';

        $newStyles = $styles;
        preg_match_all('/\d+/', $matches[0], $matches);

        foreach ($matches[0] as $code) {
            switch ($code) {
                case '0':
                    // Reset all styles.
                    $newStyles['background'] = null;
                    $newStyles['blink'] = false;
                    $newStyles['bold'] = false;
                    $newStyles['color'] = null;
                    //$newStyles['inverse']      = false;
                    $newStyles['italic'] = false;
                    $newStyles['line-through'] = false;
                    $newStyles['underline'] = false;
                    break;
                case '1':
                    $newStyles['bold'] = true;
                    break;
                case '3':
                    $newStyles['italic'] = true;
                    break;
                case '4':
                case '21':
                    $newStyles['underline'] = true;
                    break;
                case '5':
                case '6':
                    $newStyles['blink'] = true;
                    break;
                case '9':
                    $newStyles['line-through'] = true;
                    break;
                case '2':
                case '22':
                    $newStyles['bold'] = false;
                    break;
                case '23':
                    $newStyles['italic'] = false;
                    break;
                case '24':
                    $newStyles['underline'] = false;
                    break;
                case '25':
                    $newStyles['blink'] = false;
                    break;
                case '29':

                    $newStyles['line-through'] = false;
                    break;
                case '30':
                case '31':
                case '32':
                case '33':
                case '34':
                case '35':
                case '36':
                case '37':
                    $newStyles['color'] = $code - 30;
                    break;
                case '39':
                    $newStyles['color'] = null;
                    break;
                case '40':
                case '41':
                case '42':
                case '43':
                case '44':
                case '45':
                case '46':
                case '47':
                    $newStyles['background'] = $code - 40;
                    break;
                case '49':
                    $newStyles['background'] = null;
                    break;
                default:
                    break;
            }
        }

        if ($newStyles === $styles) return '';

        $styles = $newStyles;

        $html = $css ? '</span>' : '';

        $css = '';

        if (!is_null($styles['background']))
            $css .= ($css ? '; ' : '') . "background-color: " . self::$colors[$styles['background']] . " ";

        if ($styles['blink'] || $styles['line-through'] || $styles['underline']) {
            $css .= ($css ? '; ' : '') . 'text-decoration:';
            if ($styles['blink']) $css .= ' blink';
            if ($styles['line-through']) $css .= ' line-through';
            if ($styles['underline']) $css .= ' underline';
        }

        if ($styles['bold'] && is_null($styles['color'])) $css .= ($css ? '; ' : '') . 'font-weight: bold';

        if (!is_null($styles['color']))
            $css .= ($css ? '; ' : '') . "color: " . self::$colors[$styles['color'] | $styles['bold'] << 3] . " ";

        if ($styles['italic']) $css .= ($css ? '; ' : '') . 'font-style: italic';

        if ($css) {
            $html .= "<span style=\"$css\">";
        }
        return $html;
    }
}
