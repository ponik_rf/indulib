<?php

namespace Ponikrf\Indulib\Helpers;

/**
 * Класс для упращенной работы с pid файлами
 *
 * @author ponikrf <ponik_rf@mail.ru>
 */
class PidFile
{
    /**
     * Директория пид файла
    */
    protected static $pidFileDir = './';

    /**
     * Имя файла
    */
    protected static $pidFileName = 'pid.lock';

    /**
     * Возвращает ID pid процесса или 0 в случае если процесс уже мертв
     *
     * @return int
    */
    public static function checkPid(){
        if (file_exists(self::$pidFileDir.self::$pidFileName)){
            $pid = (int) file_get_contents(self::$pidFileDir.self::$pidFileName);
            if (posix_kill($pid, SIG_DFL)) {
                return $pid;
            } else {
                unlink(self::$pidFileDir.self::$pidFileName);
                return 0;
            }
        }else{
            return 0;
        }
    }

    public static function createPid(){
        $pid = getmypid();
        $fpid = fopen(self::$pidFileDir.self::$pidFileName, 'wb');
        fwrite($fpid, $pid);
        fclose($fpid);
    }

    public static function deletePid(){
        unlink(self::$pidFileDir.self::$pidFileName);
    }
}